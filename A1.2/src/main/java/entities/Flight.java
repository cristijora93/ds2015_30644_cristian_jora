package entities;

import utils.Dateutils;

import javax.persistence.GeneratedValue;
import java.util.Date;

/**
 * Created by cristi on 10/11/2015.
 */
public class Flight {
    @GeneratedValue
    private int id;
    private String flight_number;
    private String airplane_type;
    private String departure_city;
    private Date departure_date;
    private String departure_dateString;
    private String arrival_city;
    private Date arrival_date;
    private String arrival_dateString;

    public void setId(int id) {
        this.id = id;
    }

    public void setFlight_number(String flight_number) {
        this.flight_number = flight_number;
    }

    public void setAirplane_type(String airplane_type) {
        this.airplane_type = airplane_type;
    }

    public void setDeparture_city(String departure_city) {
        this.departure_city = departure_city;
    }

    public void setDeparture_date(Date departure_date) {
        this.departure_date = departure_date;
    }

    public void setArrival_city(String arrival_city) {
        this.arrival_city = arrival_city;
    }

    public void setArrival_date(Date arrival_date) {
        this.arrival_date = arrival_date;
    }

    public int getId() {
        return id;
    }

    public String getFlight_number() {
        return flight_number;
    }

    public String getAirplane_type() {
        return airplane_type;
    }

    public String getDeparture_city() {
        return departure_city;
    }

    public Date getDeparture_date() {
        return departure_date;
    }

    public String getArrival_city() {
        return arrival_city;
    }

    public Date getArrival_date() {
        return arrival_date;
    }


    public void setDeparture_dateString(String departure_dateString) {
        this.departure_dateString = departure_dateString;
    }

    public void setArrival_dateString(String arrival_dateString) {
        this.arrival_dateString = arrival_dateString;
    }

    public String getDeparture_dateString() {
        if(departure_date!=null){
            return Dateutils.convertToString(departure_date);
        }
       else return departure_dateString;
    }

    public String getArrival_dateString() {
        if(arrival_date!=null){
            return Dateutils.convertToString(arrival_date);
        }
        else return arrival_dateString;
    }
}
