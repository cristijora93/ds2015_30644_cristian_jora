package entities;

import utils.Dateutils;

import java.util.Date;

/**
 * Created by cristi on 10/28/2015.
 */
public class FlightDate {
    private Date departureDate;
    private Date arrivalDate;
    private String departureDateString;
    private String arrivalDateString;

    public String getArrivalDateString() {
        if(arrivalDate!=null)
        return  Dateutils.convertToString(arrivalDate);
        else return "";
    }

    public void setArrivalDateString(String arrivalDateString) {
        this.arrivalDateString = arrivalDateString;
    }

    public String getDepartureDateString() {
        if(departureDate!=null)
        return Dateutils.convertToString(departureDate);
        else return "";
    }

    public void setDepartureDateString(String departureDateString) {
        this.departureDateString = departureDateString;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }
}
