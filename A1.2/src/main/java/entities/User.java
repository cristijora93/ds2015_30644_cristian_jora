package entities;

/**
 * Created by cristi on 10/12/2015.
 */
public class User {
    private int id;
    private String username;
    private String password;
    private String role;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {

        return id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUsername() {

        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getRole() {
        return role;
    }
}
