package filters;


import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by cristi on 10/13/2015.
 */
@WebFilter({"/admin/*","/user/*"})
public class AuthenticationFilter implements Filter {
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        String redirectLink = "";

        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse res = (HttpServletResponse) servletResponse;
        req.getSession().setAttribute("requestURI", req.getRequestURI());

        Object requestUserRole = req.getSession().getAttribute("userRole");
        Object requestUserAuthenticated = req.getSession().getAttribute("isAuthenticated");

        if (requestUserRole != null && requestUserAuthenticated != null) {
            if (requestUserRole.equals("admin") && requestUserAuthenticated.equals("true")) {
                filterChain.doFilter(servletRequest, servletResponse);
            }
            if (requestUserRole.equals("user") && requestUserAuthenticated.equals("true")) {
                String requestURI=  req.getSession().getAttribute("requestURI").toString();
                if(requestURI.startsWith("/admin")){
                    res.sendRedirect("/permissions.jsp");
                }
                else{
                    filterChain.doFilter(servletRequest, servletResponse);
                }

            }
        } else {

            res.sendRedirect("/login");
        }
        req.getSession().removeAttribute("requestURI");

    }

    public void destroy() {

    }
}
