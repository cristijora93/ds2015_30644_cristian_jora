package BusinessLogic;

import dao.EntityDAO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import utils.HibernateUtils;

import java.util.List;

/**
 * Created by cristi on 10/24/2015.
 */
public class Manager<T> {

    private EntityDAO<T> entityDAO;
    private static final Log LOGGER = LogFactory.getLog(Manager.class);

    Manager() throws HibernateException {
        if (HibernateUtils.getSessionFactory() != null) {
            entityDAO = new EntityDAO<T>(HibernateUtils.getSessionFactory());
        } else {
            throw new HibernateException("Could not connect to database");
        }
    }

    public List<T> getAll(Class<T> type) {
        return entityDAO.findAll(type);
    }

    public T findById(int id, Class<T> type) {
        return entityDAO.findById(id, type);
    }

    public T getByField(Class<T> type, String fieldName, String fieldValue) {
        return entityDAO.getByField(type, fieldName, fieldValue);
    }

    public T delete(int id, Class<T> type) {
        return entityDAO.delete(id, type);
    }

    public boolean saveOrUpdate(T entity) {
        return entityDAO.saveOrUpdate(entity);
    }

}
