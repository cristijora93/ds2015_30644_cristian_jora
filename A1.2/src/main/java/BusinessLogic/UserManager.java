package BusinessLogic;

import entities.User;

/**
 * Created by cristi on 10/24/2015.
 */
public class UserManager extends Manager<User> {

    private User currentUser;
    public UserManager(){
        super();
    }

    public boolean verifyUser(String username,String password){
        boolean isUserCorrect=false;
        User user = getByField(User.class,"username",username);
        if(user!=null) {
            if (user.getUsername().equals(username) && user.getPassword().equals(password)) {
                isUserCorrect = true;
                setCurrentUser(user);
            }
        }
        return isUserCorrect;
    }

    private void setCurrentUser(User user){
        currentUser=user;
    }

    public User getCurrentUser(){
        return currentUser;
    }

    public boolean isCurrentUserAdmin(){
        boolean isUserAdmin=false;
        if(currentUser!=null){
            if(currentUser.getRole().equals("admin")){
                isUserAdmin=true;
            }
        }
        return isUserAdmin;
    }
}
