package BusinessLogic;

import entities.Flight;
import entities.FlightDate;
import utils.Dateutils;

import java.util.Date;

/**
 * Created by cristi on 10/24/2015.
 */
public class FlightManager extends Manager<Flight> {

    public FlightManager() {
        super();
    }

    public FlightDate getLocalDates(Flight flight) throws IndexOutOfBoundsException {
        FlightDate flightDate = new FlightDate();

        Date localDepartureTime = Dateutils.getLocalTime(flight.getDeparture_city(), flight.getDeparture_date());
        Date localArrivalTime = Dateutils.getLocalTime(flight.getArrival_city(), flight.getArrival_date());
        flightDate.setArrivalDate(localArrivalTime);
        flightDate.setDepartureDate(localDepartureTime);

        return flightDate;
    }

    public String validateFlight(Flight flight) {
        String error = "";
        if (flight.getArrival_date() == null) {
            error = "Arrival date cannot be left empty";
        }
        if (flight.getDeparture_date() == null) {
            error = "Departure date cannot be left empty";
        }
        if (flight.getArrival_city().equals("")) {
            error = "Arrival city cannot be left empty";
        }
        if (flight.getDeparture_city().equals("")) {
            error = "Departure city cannot be left empty";
        }
        if (flight.getFlight_number().equals("")) {
            error = "Flight number cannot be left empty";
        }
        return error;
    }

}
