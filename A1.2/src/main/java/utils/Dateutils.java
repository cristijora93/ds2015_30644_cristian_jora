package utils;

import com.google.maps.GeoApiContext;
import com.google.maps.TimeZoneApi;
import com.google.maps.model.LatLng;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by cristi on 10/25/2015.
 */
public class Dateutils {
    private static GeoApiContext context = new GeoApiContext().setApiKey("AIzaSyBRRCWhOAOMTQQbT0UygCeS-WFh1exm4Ek");

    public static Date getLocalTime(String city, Date utcDate) {

        com.google.code.geocoder.model.LatLng location = GeoCodeUtils.getCityCoordinates(city);
        LatLng location2 = Dateutils.getLocation(location.getLat(), location.getLng());
        Date localTime = Dateutils.getLocalTimeFromCoordinates(location2, utcDate);
        return localTime;
    }

    private static Date getLocalTimeFromCoordinates(LatLng location, Date date) {
        Date newDate=new Date();
        TimeZone timeZone = TimeZoneApi.getTimeZone(context, location).awaitIgnoreError();
        long time = date.getTime();
        time += timeZone.getRawOffset() + timeZone.getDSTSavings();
        newDate.setTime(time);
        return newDate;
    }

    private static LatLng getLocation(BigDecimal latitude, BigDecimal longitude) {
        return new LatLng(latitude.doubleValue(), longitude.doubleValue());
    }

    public static Date convertToDate(String date) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date result = null;

        try {
            result = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static String convertToString(Date date) {
        String result;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        result = df.format(date);
        return result;
    }
}
