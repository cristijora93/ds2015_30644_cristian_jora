package utils;

import entities.Flight;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * Created by cristi on 10/25/2015.
 */
public class ServletUtils {

    public static Flight getFlightFromRequest(HttpServletRequest request) {
        Flight flight = new Flight();
        String flight_id = request.getParameter("flight_id");
        String flight_number = request.getParameter("flight_number");
        String airplane_type = request.getParameter("airplane_type");
        String departure_city = request.getParameter("departure_city");
        String departure_date = request.getParameter("departure_date");
        String arrival_city = request.getParameter("arrival_city");
        String arrival_date = request.getParameter("arrival_date");
        int flightId = 0;
        Date departure = null;
        Date arrival = null;
        if (!StringUtils.isEmpty(flight_id)) {
            flightId = Integer.parseInt(flight_id);
        }
        if (departure_date != null && arrival_date != null) {
            departure = Dateutils.convertToDate(departure_date);
            arrival = Dateutils.convertToDate(arrival_date);

        }
        flight.setId(flightId);
        flight.setFlight_number(flight_number);
        flight.setAirplane_type(airplane_type);
        flight.setArrival_city(arrival_city);
        flight.setArrival_date(arrival);
        flight.setDeparture_city(departure_city);
        flight.setDeparture_date(departure);
        return flight;

    }

}
