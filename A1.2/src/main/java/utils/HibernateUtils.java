package utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Created by cristi on 10/11/2015.
 */
public class HibernateUtils {
    private static final Log LOGGER = LogFactory.getLog(HibernateUtils.class);
    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null)
            try {
                sessionFactory = new Configuration().configure().buildSessionFactory();
            } catch (HibernateException ex) {
                LOGGER.error(ex);
            }
        return sessionFactory;
    }
}
