package utils;

import com.google.code.geocoder.Geocoder;
import com.google.code.geocoder.GeocoderRequestBuilder;
import com.google.code.geocoder.model.GeocodeResponse;
import com.google.code.geocoder.model.GeocoderRequest;
import com.google.code.geocoder.model.LatLng;

import java.io.IOException;

/**
 * Created by cristian.jora on 28.10.2015.
 */
class GeoCodeUtils {

    public static LatLng getCityCoordinates(String city){
        LatLng location=null;
        final Geocoder geocoder = new Geocoder();
        GeocoderRequest geocoderRequest = new GeocoderRequestBuilder().setAddress(city).setLanguage("en").getGeocoderRequest();
        try {

            GeocodeResponse geocodeResponse = geocoder.geocode(geocoderRequest);
            location=geocodeResponse.getResults().get(0).getGeometry().getLocation();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return location;

    }
   
}
