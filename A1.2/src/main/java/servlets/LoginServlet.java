package servlets;

import BusinessLogic.UserManager;
import entities.User;
import org.hibernate.HibernateException;
import utils.EncryptUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by cristi on 10/11/2015.
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    private UserManager userManager;
    private String error;

    public LoginServlet() {
    }

    public void init() {
        try {
            userManager = new UserManager();
        } catch (HibernateException ex) {
            error = ex.getMessage();
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (userManager == null) {
            request.setAttribute("error", error);
            request.getRequestDispatcher("/error.jsp").
                    forward(request, response);
            return;
        }
        String requestUserName = (request.getParameter("username"));
        String requestPassword = (request.getParameter("password"));
        requestPassword = EncryptUtils.getMD5(requestPassword);
        String redirectLink = "";
        if (userManager.verifyUser(requestUserName, requestPassword)) {
            User user = userManager.getCurrentUser();
            HttpSession session = request.getSession();
            session.setAttribute("user", user.getUsername());
            session.setAttribute("userRole", user.getRole());
            session.setAttribute("isAuthenticated", "true");

            Object requestURI = session.getAttribute("requestURI");
            request.getSession().removeAttribute("requestURI");
            if (requestURI != null) {
                redirectLink = requestURI.toString();
            } else {
                if (userManager.isCurrentUserAdmin()) {
                    redirectLink = "admin/flights";
                } else {
                    redirectLink = "/home";
                }
            }
        } else {
            request.setAttribute("error","Username or password is incorrect");
            request.getSession().removeAttribute("requestURI");
            doGet(request,response);
        }
        response.sendRedirect(redirectLink);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/Login.jsp").
                forward(request, response);
    }
}
