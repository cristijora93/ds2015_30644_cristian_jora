package servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by cristi on 10/11/2015.
 */
@WebServlet("/home")
public class HomeServlet extends HttpServlet {
    public HomeServlet() {
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Object requestUserAuthenticated = request.getSession().getAttribute("isAuthenticated");
        if (requestUserAuthenticated != null) {
            Object requestUserRole = request.getSession().getAttribute("userRole");
            if (requestUserRole.equals("admin") && requestUserAuthenticated.equals("true")) {
              response.sendRedirect("admin/flights");
            }
            else{
                response.sendRedirect("index.jsp");
            }

        } else {
            response.sendRedirect("index.jsp");
        }
    }
}
