package servlets.admin_servlets;

import BusinessLogic.FlightManager;
import entities.Flight;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by cristi on 10/11/2015.
 */
@WebServlet("/admin/flights")
public class AdminFlightServlet extends HttpServlet {
    private FlightManager flightManager;

    public AdminFlightServlet() {
    }

    public void init() {
        flightManager=new FlightManager();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Flight> flights=flightManager.getAll(Flight.class);
        request.setAttribute("flights",flights);
        request.getRequestDispatcher("/admin/AdminFlights.jsp").
                forward(request, response);
    }
}
