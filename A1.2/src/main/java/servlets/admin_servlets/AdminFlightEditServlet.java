package servlets.admin_servlets;

import BusinessLogic.FlightManager;
import entities.Flight;
import utils.ServletUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by cristi on 10/11/2015.
 */
@WebServlet("/admin/flightEdit")
public class AdminFlightEditServlet extends HttpServlet {
    private FlightManager flightManager;

    public AdminFlightEditServlet() {
    }

    public void init() {
        flightManager = new FlightManager();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Flight flight = ServletUtils.getFlightFromRequest(request);
        String error = flightManager.validateFlight(flight);
        request.setAttribute("flight", flight);
        request.setAttribute("flightId", flight.getId());
        if (error == "") {
            error = "Could not save the information";
            if (flightManager.saveOrUpdate(flight)) {
                response.sendRedirect("flights");
            } else {

                request.setAttribute("error", error);
                doGet(request, response);
            }
        } else {
            request.setAttribute("error", error);
            doGet(request, response);
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Flight flight;
        String flightId = request.getParameter("flightId");
        request.setAttribute("flightId", flightId);
        if (flightId != null) {
            int id = Integer.parseInt(flightId);
            flight = flightManager.findById(id, Flight.class);
            request.setAttribute("flight", flight);
        }
        request.getRequestDispatcher("/admin/AdminFlightEdit.jsp").
                forward(request, response);
    }

}
