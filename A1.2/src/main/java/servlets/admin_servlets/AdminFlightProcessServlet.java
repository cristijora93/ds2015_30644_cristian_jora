package servlets.admin_servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by cristi on 10/11/2015.
 */
@WebServlet("/admin/flightProcess")
public class AdminFlightProcessServlet extends HttpServlet {
    public AdminFlightProcessServlet() {
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String actionType=request.getParameter("flight_action");
        if(actionType.equals("Edit")){
            request.getRequestDispatcher("flightEdit").
                    forward(request, response);
        }
        else{
            request.getRequestDispatcher("flightDelete").
                    forward(request, response);
        }

    }

}
