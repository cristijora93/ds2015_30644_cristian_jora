package servlets.admin_servlets;

import BusinessLogic.FlightManager;
import entities.Flight;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by cristi on 10/11/2015.
 */
@WebServlet("/admin/flightDelete")
public class AdminFlightDeleteServlet extends HttpServlet {
    private FlightManager flightManager;

    public AdminFlightDeleteServlet() {
    }

    public void init() {
        flightManager = new FlightManager();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Flight flight = null;
        String flightId = request.getParameter("flightId");
        request.setAttribute("flightId", flightId);
        if (flightId != null) {
            int id = Integer.parseInt(flightId);
            flight = flightManager.delete(id, Flight.class);
        }

        response.sendRedirect("flights");
    }

}
