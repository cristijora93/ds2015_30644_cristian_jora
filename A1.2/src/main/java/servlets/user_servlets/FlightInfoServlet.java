package servlets.user_servlets;

import BusinessLogic.FlightManager;
import entities.Flight;
import entities.FlightDate;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by cristi on 10/11/2015.
 */
@WebServlet("/user/FlightInfo")
public class FlightInfoServlet extends HttpServlet {

    private FlightManager flightManager;

    @Override
    public void init() throws ServletException {
        flightManager = new FlightManager();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String flightId = request.getParameter("flightId");
        if (flightId != null) {
            int id = Integer.parseInt(flightId);
            Flight flight = flightManager.findById(id, Flight.class);
            try {
                FlightDate flightDate = flightManager.getLocalDates(flight);
                request.setAttribute("localTimeDepartureCity", flightDate.getDepartureDateString());
                request.setAttribute("localTimeArrivalCity", flightDate.getArrivalDateString());
            } catch (Exception ex) {
                request.setAttribute("error","Something went wrong!");
                request.getRequestDispatcher("../error.jsp").forward(request, response);
                return;
            }

            request.setAttribute("flight", flight);
        }
        request.getRequestDispatcher("FlightInfo.jsp").forward(request, response);
    }
}
