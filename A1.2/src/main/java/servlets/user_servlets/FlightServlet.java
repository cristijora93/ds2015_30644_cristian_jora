package servlets.user_servlets;

import BusinessLogic.FlightManager;
import BusinessLogic.Manager;
import entities.Flight;
import org.hibernate.HibernateException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by cristi on 10/11/2015.
 */
@WebServlet("/user/flights")
public class FlightServlet extends HttpServlet {
    private Manager<Flight> flightManager;

    public FlightServlet() {
    }

    public void init() {
        try {
            flightManager = new FlightManager();
        } catch (HibernateException ex) {
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Flight> flights = flightManager.getAll(Flight.class);
        request.setAttribute("flights", flights);
        request.getRequestDispatcher("Flights.jsp").
                forward(request, response);
    }
}
