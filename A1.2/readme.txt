1.Initialize git for a specific folder
2.Add remote from https://cristijora93@bitbucket.org/cristijora93/ds2015_30644_cristian_jora.git
3.Open the project with IntelliJ IDEA.
4.Make sure that the Tomcat Server is set correctly under Run -> Edit configurations.
5.In case the home folder for Tomcat is not the same, please specify the folder to Tomcat Home.
6.Import the sql file "assignment-one-db.sql" in MySql Workbench.
7.If needed, change the username and password from hibernate.cfg.xml.
8.Run the application with Tomcat Server.