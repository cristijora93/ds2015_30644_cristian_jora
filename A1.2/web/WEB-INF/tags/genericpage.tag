<%@tag description="Overall Page template" pageEncoding="UTF-8" %>
<%@attribute name="header" fragment="true" %>
<%@attribute name="footer" fragment="true" %>
<html>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="/css/Default.css">
<body>
<div id="pageheader">
    <input type="hidden" id="loginField" name="flightId" value="$<%= session.getAttribute("isAuthenticated") %>">
    <input type="hidden" id="userRoleField" name="userRole" value="$<%= session.getAttribute("userRole") %>">
    <div id="custom-bootstrap-menu" class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header"><a class="navbar-brand" href="#">Assignment 1</a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-menubuilder">
                    <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span
                        class="icon-bar"></span><span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse navbar-menubuilder">
                <ul class="nav navbar-nav navbar-left">
                    <li><a  href="/home">Home</a>
                    </li>
                    <li><a id="flightInfoBtn" href="/user/flights" >FlightInfo</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a id="loginLink" href="/login">Login</a>
                </li>
                    <li><a id="signOutLink" href="/signout">Sign out</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <jsp:invoke fragment="header"/>
</div>
<div id="bodyContent">
    <jsp:doBody/>
</div>
<div id="pagefooter">
    <jsp:invoke fragment="footer"/>
</div>
</body>
</html>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script type="text/javascript">
    $( document ).ready(function() {
        var loggedIn=$("#loginField").val();
        var userRole=$("#userRoleField").val();
        if(userRole == "$admin"){
            $("#flightInfoBtn").removeClass("invisible");
        }
        else{
            $("#flightInfoBtn").addClass("invisible");
        }

        if(loggedIn=="$true"){
            $("#loginLink").addClass("invisible");
            $("#signOutLink").removeClass("invisible");
        }
        else{
            $("#loginLink").removeClass("invisible");
            $("#signOutLink").addClass("invisible");
        }
    });

</script>