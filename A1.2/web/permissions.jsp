<%--
  Created by IntelliJ IDEA.
  User: cristi
  Date: 10/14/2015
  Time: 11:10 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage>
  <jsp:body>
      <div class="alert alert-danger" role="alert">You do not have permissions to acces this page!</div>
  </jsp:body>
</t:genericpage>
