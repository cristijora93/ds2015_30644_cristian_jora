<%--
  Created by IntelliJ IDEA.
  User: cristi
  Date: 10/12/2015
  Time: 9:32 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:genericpage>
    <jsp:body>
        <div class="row">
            <div class="col-md-4">
            </div>
            <div class="col-md-2">
                <h3><span class="label label-warning">Departure Time for ${flight.departure_city} !</span></h3>

                <h3><span class="label label-success">UTC: ${flight.departure_date} !</span></h3>

                <h3><span class="label label-success">Local Time: ${localTimeDepartureCity} !</span></h3>
            </div>
            <div class="col-md-2">
                <h3><span class="label label-warning">Arrival Time for ${flight.arrival_city} !</span></h3>

                <h3><span class="label label-success">UTC: ${flight.arrival_date} ! </span></h3>

                <h3><span class="label label-success">Local Time: ${localTimeArrivalCity} !</span></h3>
            </div>
        </div>
    </jsp:body>
</t:genericpage>
