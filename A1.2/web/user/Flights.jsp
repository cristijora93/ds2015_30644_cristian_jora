<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: cristi
  Date: 10/11/2015
  Time: 8:30 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage>
    <jsp:body>
        <table class="table table-stripped table-bordered table-hover" id="flightsTable">
            <thead>
            <th>#</th>
            <th>Flight nr.</th>
            <th>Airplane type</th>
            <th>Departure city</th>
            <th>Departure date</th>
            <th>Arrival city</th>
            <th>Arrival date</th>
            <th>Flight details</th>
            </thead>
            <tbody>
            <c:forEach items="${flights}" var="flight" varStatus="loop">
                <form action="FlightInfo" method="GET">
                <tr>
                    <th>${loop.index+1}</th>
                        <input type="hidden" id="flight${flight.id}" name="flightId" value="${flight.id}">
                    <td><c:out value="${flight.flight_number}"/></td>
                    <td><c:out value="${flight.airplane_type}"/></td>
                    <td><c:out value="${flight.departure_city}"/></td>
                    <td><c:out value="${flight.departure_date}"/></td>
                    <td><c:out value="${flight.arrival_city}"/></td>
                    <td><c:out value="${flight.arrival_date}"/></td>
                    <td><input type="submit" value="Check" class="btn btn-info"></td>
                </tr>
                </form>
            </c:forEach>
            </tbody>
        </table>
    </jsp:body>
</t:genericpage>
