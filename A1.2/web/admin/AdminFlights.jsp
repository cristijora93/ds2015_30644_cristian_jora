<%--
  Created by IntelliJ IDEA.
  User: cristi
  Date: 10/12/2015
  Time: 10:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:genericpage>
  <jsp:body>
    <a href="flightCreate">
    <input type="submit" value="Create Flight" class="btn btn-info">
    </a>
    <div class="row top-buffer"></div>
    <table class="table table-stripped table-bordered table-hover">
      <thead>
      <th>Nr</th>
      <th>Flight nr.</th>
      <th>Airplane type</th>
      <th>Departure city</th>
      <th>Departure date</th>
      <th>Arrival city</th>
      <th>Arrival date</th>
      <th>Edit flight</th>
      <th>Delete flight</th>
      </thead>
      <tbody>
      <c:forEach items="${flights}" var="flight" varStatus="loop">
        <form id="flightData" action="flightProcess" method="GET">
          <tr>
            <th>${loop.index+1}</th>
            <input type="hidden" id="flight${flight.id}" name="flightId" value="${flight.id}">
            <td><c:out value="${flight.flight_number}"/></td>
            <td><c:out value="${flight.airplane_type}"/></td>
            <td><c:out value="${flight.departure_city}"/></td>
            <td><c:out value="${flight.departure_date}"/></td>
            <td><c:out value="${flight.arrival_city}"/></td>
            <td><c:out value="${flight.arrival_date}"/></td>
            <td>
              <input type="submit" value="Edit"  class="btn btn-info" name="flight_action">
            </td>
            <td>
              <input id="DeleteBtn" type="submit" value="Delete" name="flight_action" class="btn btn-danger">
            </td>
          </tr>
        </form>
      </c:forEach>
      </tbody>
    </table>
  </jsp:body>
</t:genericpage>
