<%--
  Created by IntelliJ IDEA.
  User: cristi
  Date: 10/15/2015
  Time: 8:35 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<t:genericpage>
    <jsp:body>
        <form action="flightEdit" method="post">
            <jsp:include page="FlightEditTemplate.jsp"/>
            <h3><span class="label label-danger">${requestScope.get("error")}</span></h3>
        </form>
    </jsp:body>
</t:genericpage>


