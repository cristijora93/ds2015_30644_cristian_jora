<%--
  Created by IntelliJ IDEA.
  User: cristi
  Date: 10/15/2015
  Time: 8:35 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="form-group">
    <label for="input_flight_number">Flight Number</label>
    <input type="text" class="form-control" id="input_flight_number" name="flight_number"
           placeholder="Flight Number" value="${flight.flight_number}">
</div>
<div class="form-group">
    <label for="input_airplane_type">Airplane Type</label>
    <input type="text" class="form-control" id="input_airplane_type" name="airplane_type"
           placeholder="Airplane Type" value="${flight.airplane_type}">
</div>
<div class="form-group">
    <label for="input_departure_city">Departure City</label>
    <input type="text" class="form-control" id="input_departure_city" name="departure_city"
           placeholder="Departure City" value="${flight.departure_city}">
</div>
<div class="form-group">
    <label for="input_departure_date">Departure Date</label>
    <input type="text" class="form-control" id="input_departure_date" name="departure_date"
           placeholder="Departure Date" value="${flight.departure_dateString}">
</div>
<div class="form-group">
    <label for="input_arrival_city">Arrival City</label>
    <input type="text" class="form-control" id="input_arrival_city" name="arrival_city"
           placeholder="Arrival City" value="${flight.arrival_city}">
</div>
<div class="form-group">
    <label for="input_arrival_date">Arrival Date</label>
    <input type="text" class="form-control" id="input_arrival_date" name="arrival_date"
           placeholder="Arrival Date" value="${flight.arrival_dateString}">
</div>
<input type="hidden" name="flight_id" value="${flight.id}">
<button class="btn btn-default" id="flightSubmit">Submit</button>

