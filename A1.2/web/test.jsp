<%--
  Created by IntelliJ IDEA.
  User: cristi
  Date: 10/11/2015
  Time: 3:46 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
</head>
<body>

<table class="table table-stripped">
    <thead>
    <th>#</th>
    <th>ID</th>
    <th>First Name</th>
    </thead>
    <tbody>
    <c:forEach items="${students}" var="student">

        <tr>
            <th>Nr</th>
            <td>Student ID: <c:out value="${student.id}"/></td>
            <td>Student name: <c:out value="${student.firstname}"/></td>
        </tr>

    </c:forEach>
    </tbody>
</table>

</body>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</html>
