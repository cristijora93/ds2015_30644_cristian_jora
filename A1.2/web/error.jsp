<%--
  Created by IntelliJ IDEA.
  User: cristian.jora
  Date: 28.10.2015
  Time: 13:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage>
  <jsp:body>
    <div class="alert alert-danger" role="alert">${error}</div>
  </jsp:body>
</t:genericpage>
