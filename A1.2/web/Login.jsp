<%--
  Created by IntelliJ IDEA.
  User: cristi
  Date: 10/13/2015
  Time: 8:24 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:genericpage>
  <jsp:body>
    <form action="${pageContext.request.contextPath}/login" method="post">
      <div class="form-group">
        <label for="inputUser">Username</label>
        <input type="text" class="form-control" id="inputUser" name="username" placeholder="Username">
      </div>
      <div class="form-group">
        <label for="inputPassword">Password</label>
        <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password">
      </div>
      <button type="submit" class="btn btn-default">Submit</button>
    </form>
    <h4><span class="label label-danger">${error}</span></h4>
  </jsp:body>
</t:genericpage>
