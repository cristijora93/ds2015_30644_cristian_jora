using System;
using System.ComponentModel;
using System.Runtime.Remoting.Channels.Tcp;
using System.Windows.Forms;
using RemotableObjects;
using static System.Runtime.Remoting.Channels.ChannelServices;

namespace RClient
{


    public class ClientGui : Form
    {
        private readonly ITaxService _taxService;
        private Button _btnComputeTax;
        private TextBox _tbEngineCapacity;
        private Label _label2;
        private Label _lblTaxResult;
        private Label _lblErrorTax;
        private Label _lblErrorPrice;
        private Label _lblPurcahsePrice;
        private TextBox _tbPurchasePrice;
        private Button _btnComputePrice;
        private Label _lblYear;
        private TextBox _tbYear;
        private Label _lblPriceResult;
        private readonly Container _components = null;

        public ClientGui()
        {

            InitializeComponent();

            //************************************* TCP *************************************//
            // using TCP protocol
            // running both client and server on same machines
            TcpChannel chan = new TcpChannel();
            RegisterChannel(chan);
            // Create an instance of the remote object
            _taxService = (TaxService)Activator.GetObject(typeof(TaxService), "tcp://localhost:8080/Assignment2");
            // if remote object is on another machine the name of the machine should be used instead of localhost.
            //************************************* TCP *************************************//
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _components?.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._btnComputeTax = new System.Windows.Forms.Button();
            this._tbEngineCapacity = new System.Windows.Forms.TextBox();
            this._label2 = new System.Windows.Forms.Label();
            this._lblTaxResult = new System.Windows.Forms.Label();
            this._lblErrorTax = new System.Windows.Forms.Label();
            this._lblErrorPrice = new System.Windows.Forms.Label();
            this._lblPurcahsePrice = new System.Windows.Forms.Label();
            this._tbPurchasePrice = new System.Windows.Forms.TextBox();
            this._btnComputePrice = new System.Windows.Forms.Button();
            this._lblYear = new System.Windows.Forms.Label();
            this._tbYear = new System.Windows.Forms.TextBox();
            this._lblPriceResult = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _btnComputeTax
            // 
            this._btnComputeTax.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._btnComputeTax.Location = new System.Drawing.Point(128, 160);
            this._btnComputeTax.Name = "_btnComputeTax";
            this._btnComputeTax.Size = new System.Drawing.Size(121, 28);
            this._btnComputeTax.TabIndex = 1;
            this._btnComputeTax.Text = "Compute Tax";
            this._btnComputeTax.UseVisualStyleBackColor = true;
            this._btnComputeTax.Click += new System.EventHandler(this.btnComputeTax_Click);
            // 
            // _tbEngineCapacity
            // 
            this._tbEngineCapacity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._tbEngineCapacity.Location = new System.Drawing.Point(144, 44);
            this._tbEngineCapacity.Name = "_tbEngineCapacity";
            this._tbEngineCapacity.Size = new System.Drawing.Size(100, 22);
            this._tbEngineCapacity.TabIndex = 3;
            // 
            // _label2
            // 
            this._label2.AutoSize = true;
            this._label2.BackColor = System.Drawing.Color.Transparent;
            this._label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._label2.Location = new System.Drawing.Point(25, 47);
            this._label2.Name = "_label2";
            this._label2.Size = new System.Drawing.Size(109, 16);
            this._label2.TabIndex = 6;
            this._label2.Text = "Engine Capacity:";
            // 
            // _lblTaxResult
            // 
            this._lblTaxResult.AutoSize = true;
            this._lblTaxResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblTaxResult.ForeColor = System.Drawing.Color.Green;
            this._lblTaxResult.Location = new System.Drawing.Point(141, 86);
            this._lblTaxResult.Name = "_lblTaxResult";
            this._lblTaxResult.Size = new System.Drawing.Size(0, 16);
            this._lblTaxResult.TabIndex = 7;
            // 
            // _lblErrorTax
            // 
            this._lblErrorTax.AutoSize = true;
            this._lblErrorTax.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblErrorTax.ForeColor = System.Drawing.Color.Red;
            this._lblErrorTax.Location = new System.Drawing.Point(64, 211);
            this._lblErrorTax.Name = "_lblErrorTax";
            this._lblErrorTax.Size = new System.Drawing.Size(0, 18);
            this._lblErrorTax.TabIndex = 8;
            // 
            // _lblErrorPrice
            // 
            this._lblErrorPrice.AutoSize = true;
            this._lblErrorPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblErrorPrice.ForeColor = System.Drawing.Color.Red;
            this._lblErrorPrice.Location = new System.Drawing.Point(336, 211);
            this._lblErrorPrice.Name = "_lblErrorPrice";
            this._lblErrorPrice.Size = new System.Drawing.Size(0, 18);
            this._lblErrorPrice.TabIndex = 14;
            // 
            // _lblPurcahsePrice
            // 
            this._lblPurcahsePrice.AutoSize = true;
            this._lblPurcahsePrice.BackColor = System.Drawing.Color.Transparent;
            this._lblPurcahsePrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblPurcahsePrice.Location = new System.Drawing.Point(297, 48);
            this._lblPurcahsePrice.Name = "_lblPurcahsePrice";
            this._lblPurcahsePrice.Size = new System.Drawing.Size(105, 16);
            this._lblPurcahsePrice.TabIndex = 12;
            this._lblPurcahsePrice.Text = "Purchase Price: ";
            // 
            // _tbPurchasePrice
            // 
            this._tbPurchasePrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._tbPurchasePrice.Location = new System.Drawing.Point(408, 47);
            this._tbPurchasePrice.Name = "_tbPurchasePrice";
            this._tbPurchasePrice.Size = new System.Drawing.Size(100, 22);
            this._tbPurchasePrice.TabIndex = 10;
            // 
            // _btnComputePrice
            // 
            this._btnComputePrice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._btnComputePrice.Location = new System.Drawing.Point(388, 160);
            this._btnComputePrice.Name = "_btnComputePrice";
            this._btnComputePrice.Size = new System.Drawing.Size(124, 28);
            this._btnComputePrice.TabIndex = 9;
            this._btnComputePrice.TabStop = false;
            this._btnComputePrice.Text = "Compute Price";
            this._btnComputePrice.UseVisualStyleBackColor = true;
            this._btnComputePrice.Click += new System.EventHandler(this.btnComputePrice_Click);
            // 
            // _lblYear
            // 
            this._lblYear.AutoSize = true;
            this._lblYear.BackColor = System.Drawing.Color.Transparent;
            this._lblYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblYear.Location = new System.Drawing.Point(356, 81);
            this._lblYear.Name = "_lblYear";
            this._lblYear.Size = new System.Drawing.Size(40, 16);
            this._lblYear.TabIndex = 16;
            this._lblYear.Text = "Year:";
            // 
            // _tbYear
            // 
            this._tbYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._tbYear.Location = new System.Drawing.Point(407, 80);
            this._tbYear.Name = "_tbYear";
            this._tbYear.Size = new System.Drawing.Size(100, 22);
            this._tbYear.TabIndex = 15;
            // 
            // _lblPriceResult
            // 
            this._lblPriceResult.AutoSize = true;
            this._lblPriceResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblPriceResult.ForeColor = System.Drawing.Color.Green;
            this._lblPriceResult.Location = new System.Drawing.Point(356, 118);
            this._lblPriceResult.Name = "_lblPriceResult";
            this._lblPriceResult.Size = new System.Drawing.Size(0, 16);
            this._lblPriceResult.TabIndex = 18;
            // 
            // ClientGui
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(6, 15);
            this.ClientSize = new System.Drawing.Size(649, 333);
            this.Controls.Add(this._lblPriceResult);
            this.Controls.Add(this._lblYear);
            this.Controls.Add(this._tbYear);
            this.Controls.Add(this._lblErrorPrice);
            this.Controls.Add(this._lblPurcahsePrice);
            this.Controls.Add(this._tbPurchasePrice);
            this.Controls.Add(this._btnComputePrice);
            this.Controls.Add(this._lblErrorTax);
            this.Controls.Add(this._lblTaxResult);
            this.Controls.Add(this._label2);
            this.Controls.Add(this._tbEngineCapacity);
            this.Controls.Add(this._btnComputeTax);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "ClientGui";
            this.Text = "RemoteClient";
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private void btnComputeTax_Click(object sender, EventArgs e)
        {
            var currentCar = CarUtilities.GetCarFromInputs(_tbEngineCapacity.Text);
            if (currentCar == null)
            {
                _lblErrorTax.Text = "Could not process the inputs";
            }
            else
            {
                _lblErrorTax.Text = string.Empty;
                try
                {
                    var taxResult = _taxService.ComputeTax(currentCar);
                    _lblTaxResult.Text = "Tax: " + taxResult.ToString();
                }
                catch (Exception ex)
                {
                    _lblErrorTax.Text = ex.Message;
                }

            }
        }


        private void btnComputePrice_Click(object sender, EventArgs e)
        {
            var currentCar = CarUtilities.GetCarFromInputs(_tbYear.Text, _tbPurchasePrice.Text);
            if (currentCar == null)
            {
                _lblErrorPrice.Text = "Could not process the inputs";
            }
            else
            {
                _lblErrorPrice.Text = string.Empty;
                try
                {
                    var sellingPrice = _taxService.ComputePrice(currentCar);
                    sellingPrice = Math.Truncate(sellingPrice * 100) / 100;
                    _lblPriceResult.Text = "Selling Price: " + sellingPrice.ToString();
                }
                catch (Exception ex)
                {
                    _lblErrorPrice.Text = ex.Message;
                }

            }
        }

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.Run(new ClientGui());
        }
    }
}
