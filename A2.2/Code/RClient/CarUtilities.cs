﻿using RemotableObjects;

namespace RClient
{
    public class CarUtilities
    {

        public static Car GetCarFromInputs(string engineCapacityText)
        {
            int engineCapacity;
            var succes = int.TryParse(engineCapacityText, out engineCapacity);
            return succes ? new Car(engineCapacity) : null;

        }

        public static Car GetCarFromInputs(string yearText, string purchasePriceText)
        {
            int year;
            double purcahsePrice;
            var isYearParsed = int.TryParse(yearText, out year);
            var isPriceParsed = double.TryParse(purchasePriceText, out purcahsePrice);
            return (isYearParsed && isPriceParsed) ? new Car(year, purcahsePrice) : null;

        }
    }
}