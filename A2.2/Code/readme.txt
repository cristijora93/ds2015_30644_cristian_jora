1.Download the project from the repository
2.Make sure you have .NET Framework 4.0 or above installed
3.Make sure you have Visual Studio installed
3.Open RClient.sln from the RClient folder
4.Open RServer.sln from the RServer folder
5.Run Server and Client solutions from 2 separate visual studio instances
6.Make sure you don't have port 8080(default application port) blocked
7.Execute operations from the user interface of the client application
