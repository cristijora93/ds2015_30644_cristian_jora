﻿using System;

namespace RemotableObjects
{
    public class TaxService : MarshalByRefObject, ITaxService
    {
        public double ComputeTax(Car car)
        {
            if (car.EngineCapacity <= 0)
            {
                throw new ArgumentException("Engine capacity must be positive.");
            }
            var sum = 8;
            if (car.EngineCapacity > 1601) sum = 18;
            if (car.EngineCapacity > 2001) sum = 72;
            if (car.EngineCapacity > 2601) sum = 144;
            if (car.EngineCapacity > 3001) sum = 290;
            return car.EngineCapacity / 200.0 * sum;
        }
        public double ComputePrice(Car car) 
        {
            var purchasingPrice = car.Price;
            var year = car.Year;
            if (purchasingPrice <= 0)
            {
                throw new ArgumentException("Price must be positive!");
            }
            if (year > 2015)
            {
                throw new ArgumentException("Year cannot be newer than 2015");
            }
            if (year < 2008) return 0;
            return purchasingPrice - (purchasingPrice / 7) * (2015 - year);
        }
    }
}