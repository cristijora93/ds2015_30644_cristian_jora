namespace RemotableObjects
{

	public interface ITaxService
	{
	    double ComputeTax(Car car);
	    double ComputePrice(Car car);
	}
}
