﻿using System;

namespace RemotableObjects
{
    [Serializable]
    public class Car
    {
        public int EngineCapacity{ get; set; }
        public int Year{ get; set; }
        public double Price{ get; set; }

        public Car(int engineCapacity)
        {
            EngineCapacity = engineCapacity;
        }
        public Car(int year,double price)
        {
            Year = year;
            Price = price;
        }
    }
}