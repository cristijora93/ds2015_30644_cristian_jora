using System;
using System.ComponentModel;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels.Tcp;
using System.Windows.Forms;
using RemotableObjects;
using static System.Runtime.Remoting.Channels.ChannelServices;

namespace RServer
{

	public class ServerStart : Form
	{
		private TextBox _textBox1;
	
		private readonly Container _components = null;

		public ServerStart()
		{
			InitializeComponent();
			// using TCP protocol
			TcpChannel channel = new TcpChannel(8080);
            RegisterChannel(channel);
            //Register Remote objects
			RemotingConfiguration.RegisterWellKnownServiceType(typeof(TaxService), "Assignment2", WellKnownObjectMode.Singleton);
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
			    _components?.Dispose();
			}
		    base.Dispose( disposing );
		}

		private void InitializeComponent()
		{
			this._textBox1 = new TextBox();
			this.SuspendLayout();
			// 
			// _textBox1
			// 
			this._textBox1.Dock = DockStyle.Fill;
			this._textBox1.Location = new System.Drawing.Point(0, 0);
			this._textBox1.Multiline = true;
			this._textBox1.Name = "_textBox1";
			this._textBox1.ReadOnly = true;
			this._textBox1.Size = new System.Drawing.Size(656, 246);
			this._textBox1.TabIndex = 1;
			this._textBox1.Text = "Server Started";
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(656, 246);
			this.Controls.Add(this._textBox1);
			this.Name = "Form1";
			this.Text = "RemoteServer";
			this.ResumeLayout(false);

		}

		[STAThread]
		private static void Main() 
		{
			Application.Run(new ServerStart());
		}
	}
}
