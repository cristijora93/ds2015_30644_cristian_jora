﻿using System;

namespace WebConsumer.Models
{
    public class DVD
    {
        public DVD(string title, int year, double price)
        {
            Title = title;
            Year = year;
            Price = price;
        }
        public string Title { get; set; } 
        public int Year { get; set; } 
        public double Price{ get; set; }
        public override string ToString()
        {
            return "Title: " + Title + ",Year: " + Year + ",Price: " + Price;
        }
    }
}