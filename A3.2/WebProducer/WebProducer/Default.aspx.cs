﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RabbitMQ.Client;
using WebConsumer.Models;

namespace WebConsumer
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_OnClick(object sender, EventArgs e)
        {
            string title = tbTitle.Text;
            int year;
            double price;
            bool yearSucces = int.TryParse(tbYear.Text, out year);
            bool priceSucces = double.TryParse(tbYear.Text, out price);
            year = yearSucces ? year : 0;
            price = priceSucces ? price : 0;
            DVD dvd=new DVD(title,year,price);
            SendDvdData(dvd);

        }
        private void SendDvdData(DVD dvd)
        {
            var factory = new ConnectionFactory() { HostName = "localhost", Port = AmqpTcpEndpoint.UseDefaultPort, VirtualHost = "/" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "hello",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                var body = Encoding.UTF8.GetBytes(dvd.ToString());

                channel.BasicPublish(exchange: "",
                                     routingKey: "hello",
                                     basicProperties: null,
                                     body: body);
            }
        }
    }
}