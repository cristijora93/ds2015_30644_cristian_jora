﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;

namespace ConsumerClient
{
    public class EmailService
    {
        private readonly MailAddress _fromAddress;
        private readonly SmtpClient _smtp;

        public EmailService(string fromAdress, string fromName, string fromPassword)
        {
            _fromAddress = new MailAddress(fromAdress, fromName);
            _smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Credentials = new NetworkCredential(_fromAddress.Address, fromPassword),
                Timeout = 20000
            };
        }


        public void SendMail(string toAdress, string subject, string body)
        {
            var toAddress = new MailAddress(toAdress, toAdress);
            using (var message = new MailMessage(_fromAddress, toAddress)
            {
                Subject = subject,
                Body = body
            })
            {
                _smtp.Send(message);
            }
            Console.WriteLine("Email sent to "+toAddress);
        }

    }
    
}
