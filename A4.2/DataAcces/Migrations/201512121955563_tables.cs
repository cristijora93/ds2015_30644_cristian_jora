namespace DataAcces.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        ClientId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.ClientId);
            
            CreateTable(
                "dbo.Packages",
                c => new
                    {
                        PackageId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        SenderCity = c.String(),
                        DestinationCity = c.String(),
                        Tracking = c.Boolean(nullable: false),
                        Receiver_ClientId = c.Int(),
                        Sender_ClientId = c.Int(),
                    })
                .PrimaryKey(t => t.PackageId)
                .ForeignKey("dbo.Clients", t => t.Receiver_ClientId)
                .ForeignKey("dbo.Clients", t => t.Sender_ClientId)
                .Index(t => t.Receiver_ClientId)
                .Index(t => t.Sender_ClientId);
            
            CreateTable(
                "dbo.Routes",
                c => new
                    {
                        RouteId = c.Int(nullable: false, identity: true),
                        PackageId = c.Int(nullable: false),
                        City = c.String(),
                        Time = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.RouteId)
                .ForeignKey("dbo.Packages", t => t.PackageId, cascadeDelete: true)
                .Index(t => t.PackageId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Packages", "Sender_ClientId", "dbo.Clients");
            DropForeignKey("dbo.Routes", "PackageId", "dbo.Packages");
            DropForeignKey("dbo.Packages", "Receiver_ClientId", "dbo.Clients");
            DropIndex("dbo.Routes", new[] { "PackageId" });
            DropIndex("dbo.Packages", new[] { "Sender_ClientId" });
            DropIndex("dbo.Packages", new[] { "Receiver_ClientId" });
            DropTable("dbo.Routes");
            DropTable("dbo.Packages");
            DropTable("dbo.Clients");
        }
    }
}
