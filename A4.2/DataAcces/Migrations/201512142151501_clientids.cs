namespace DataAcces.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class clientids : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Packages", "Receiver_ClientId", "dbo.Clients");
            DropForeignKey("dbo.Packages", "Sender_ClientId", "dbo.Clients");
            DropIndex("dbo.Packages", new[] { "Receiver_ClientId" });
            DropIndex("dbo.Packages", new[] { "Sender_ClientId" });
            DropColumn("dbo.Packages", "ReceiverClientid");
            DropColumn("dbo.Packages", "SenderClientId");
            RenameColumn(table: "dbo.Packages", name: "Receiver_ClientId", newName: "ReceiverClientId");
            RenameColumn(table: "dbo.Packages", name: "Sender_ClientId", newName: "SenderClientId");
            AlterColumn("dbo.Packages", "SenderClientId", c => c.Int(nullable: false));
            AlterColumn("dbo.Packages", "ReceiverClientId", c => c.Int(nullable: false));
            AlterColumn("dbo.Packages", "ReceiverClientId", c => c.Int(nullable: false));
            AlterColumn("dbo.Packages", "SenderClientId", c => c.Int(nullable: false));
            CreateIndex("dbo.Packages", "SenderClientId");
            CreateIndex("dbo.Packages", "ReceiverClientId");
            AddForeignKey("dbo.Packages", "ReceiverClientId", "dbo.Clients", "ClientId", cascadeDelete: false);
            AddForeignKey("dbo.Packages", "SenderClientId", "dbo.Clients", "ClientId", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Packages", "SenderClientId", "dbo.Clients");
            DropForeignKey("dbo.Packages", "ReceiverClientId", "dbo.Clients");
            DropIndex("dbo.Packages", new[] { "ReceiverClientId" });
            DropIndex("dbo.Packages", new[] { "SenderClientId" });
            AlterColumn("dbo.Packages", "SenderClientId", c => c.Int());
            AlterColumn("dbo.Packages", "ReceiverClientId", c => c.Int());
            AlterColumn("dbo.Packages", "ReceiverClientId", c => c.String());
            AlterColumn("dbo.Packages", "SenderClientId", c => c.String());
            RenameColumn(table: "dbo.Packages", name: "SenderClientId", newName: "Sender_ClientId");
            RenameColumn(table: "dbo.Packages", name: "ReceiverClientId", newName: "Receiver_ClientId");
            AddColumn("dbo.Packages", "SenderClientId", c => c.String());
            AddColumn("dbo.Packages", "ReceiverClientid", c => c.String());
            CreateIndex("dbo.Packages", "Sender_ClientId");
            CreateIndex("dbo.Packages", "Receiver_ClientId");
            AddForeignKey("dbo.Packages", "Sender_ClientId", "dbo.Clients", "ClientId");
            AddForeignKey("dbo.Packages", "Receiver_ClientId", "dbo.Clients", "ClientId");
        }
    }
}
