namespace DataAcces.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class userRoles : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Clients", "IsAdmin", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Clients", "IsAdmin");
        }
    }
}
