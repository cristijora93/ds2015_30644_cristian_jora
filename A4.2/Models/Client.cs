﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcces.Models
{
    public class Client
    {

        public Client()
        {
            IsAdmin = false;
        }
        public int ClientId { get; set;}
        public string UserName { get; set;}
        public string Password { get; set;}
        public string Email { get; set;}
        public bool IsAdmin { get; set; }
    }
}
