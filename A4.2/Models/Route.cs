﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcces.Models
{
    public class Route
    {
        public Route()
        {
            Time=DateTime.Now;
        }
        public int RouteId { get; set; }
        public int PackageId { get; set; }  
        public string City { get; set; }    
        public DateTime Time { get; set; }    
    }
}
