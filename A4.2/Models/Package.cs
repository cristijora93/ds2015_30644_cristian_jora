﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcces.Models
{
    public class Package
    {
        public Package()
        {
            Tracking = false;
        }
        public int PackageId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string SenderCity { get; set; }
        public string DestinationCity { get; set; }
        public bool Tracking { get; set; }
        public int SenderClientId { get; set; }
        public int ReceiverClientId { get; set; }
        public virtual Client Sender { get; set; }
        public virtual Client Receiver { get; set; }
        public virtual List<Route> Routes{ get; set; }
    }
}
