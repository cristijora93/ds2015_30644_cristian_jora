﻿namespace DataAcces.Models
{
    public class PackageDto
    {

        public int PackageId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string SenderCity { get; set; }
        public string DestinationCity { get; set; }
        public string SenderClient { get; set; }
        public string ReceiverClient { get; set; }
        public bool Tracking { get; set; }
    }
}