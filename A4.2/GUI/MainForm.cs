﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using GUI.AdminService;
using GUI.ClientService;
using GUI.Properties;
using PackageDto = GUI.AdminService.PackageDto;

namespace GUI
{
    public partial class MainForm : Form
    {
        private readonly AdminServiceSoapClient _adminReference = new AdminServiceSoapClient();
        private readonly ClientServiceSoapClient _clientReference = new ClientServiceSoapClient();
        private int _currentPackageId = 0;
        private int _currentClientId=0;
        private IBindingList _list;
        private List<ClientService.PackageDto> _clientPackageDtos;
        public MainForm()
        {
            InitializeComponent();
            PackagesGridViedw.ReadOnly = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (_adminReference.VerifyUser(tbUsername.Text, tbPassword.Text))
            {
                lblError.Text = "";
                adminPanel.Dock=DockStyle.Fill;;
                adminPanel.Visible = true;
                passowrdLbl.Visible = false;
                Usernamelbl.Visible = false;
                addPackagebtn.Visible = true;
                Client client = _adminReference.GetUser(tbUsername.Text, tbPassword.Text);
                if (client.IsAdmin)
                {
                    _list = new BindingList<PackageDto>(_adminReference.GetAllPackages());
                    tbSearch.Visible = false;
                    searchBtn.Visible = false;
                }
                else
                {
                    _clientPackageDtos = _clientReference.GetAllPackages(client.ClientId);
                    _list = new BindingList<ClientService.PackageDto>(_clientPackageDtos);
                    _currentClientId = client.ClientId;
                    addPackagebtn.Visible = false;
                    tbSearch.Visible = true;
                    searchBtn.Visible = true;
                }
                PackagesGridViedw.DataSource = null;
                PackagesGridViedw.Columns.Clear();
                PackagesGridViedw.DataSource = _list;
                if (client.IsAdmin)
                {
                    AddButtonColumns(PackagesGridViedw);
                }
                else
                {
                    AddButtonColumnsForuser(PackagesGridViedw);
                }
                
            }
            else
            {
                lblError.Text = Resources.MainForm_button1_Click_Username_or_password_is_incorrect;
               
            }
        }

        private void AddButtonColumnsForuser(DataGridView packagesGridViedw)
        {
            DataGridViewButtonColumn col = new DataGridViewButtonColumn();
            col.UseColumnTextForButtonValue = true;
            col.Text = "Check package status";
            col.Name = "Check package";
            packagesGridViedw.Columns.Add(col);
        }

        private static void AddButtonColumns(DataGridView dataGridView)
        {
            DataGridViewButtonColumn col = new DataGridViewButtonColumn();
            DataGridViewButtonColumn col2 = new DataGridViewButtonColumn();
            DataGridViewButtonColumn col3 = new DataGridViewButtonColumn();
            col.UseColumnTextForButtonValue =true;
            col.Text = "Register package";
            col.Name = "Register package";
            col2.UseColumnTextForButtonValue = true;
            col2.Text = "Remove package";
            col2.Name = "Remove";
            col3.UseColumnTextForButtonValue = true;
            col3.Text = "Update package status";
            col3.Name = "Update status";
            dataGridView.Columns.Add(col);
            dataGridView.Columns.Add(col3);
            dataGridView.Columns.Add(col2);
        }

        private void DataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (PackagesGridViedw.Columns[e.ColumnIndex].Name == "Register package")
            {
                if (PackagesGridViedw.CurrentRow != null)
                {
                    var currentPackage = (PackageDto) PackagesGridViedw.CurrentRow.DataBoundItem;
                    if (!currentPackage.Tracking)
                    {
                        _adminReference.RegisterPackage(currentPackage.PackageId);
                        PackagesGridViedw.CurrentRow.Cells["Tracking"].Value = true;
                    }
                }
            }
            if (PackagesGridViedw.Columns[e.ColumnIndex].Name == "Remove")
            {
                if (PackagesGridViedw.CurrentRow != null)
                {
                    var currentPackage = (PackageDto) PackagesGridViedw.CurrentRow.DataBoundItem;
                    _adminReference.RemovePackage(currentPackage.PackageId);
                }
                if (PackagesGridViedw.CurrentRow != null) PackagesGridViedw.Rows.Remove(PackagesGridViedw.CurrentRow);
            }
            if (PackagesGridViedw.Columns[e.ColumnIndex].Name == "Update status")
            {
                if (PackagesGridViedw.CurrentRow != null)
                {
                    var currentPackage = (PackageDto)PackagesGridViedw.CurrentRow.DataBoundItem;
                    _currentPackageId = currentPackage.PackageId;
                }
                UpdateStatusPanel.Dock=DockStyle.Fill;
                UpdateStatusPanel.Visible = true;
            }
            if (PackagesGridViedw.Columns[e.ColumnIndex].Name != "Check package") return;
            {
                PackageStatusPanel.Dock=DockStyle.Fill;
                PackageStatusPanel.Visible = true;
                adminPanel.Visible = false;
                if (PackagesGridViedw.CurrentRow == null) return;
                var currentPackage = (ClientService.PackageDto)PackagesGridViedw.CurrentRow.DataBoundItem;
                packageStatusGridView.DataSource = _clientReference.GetAllRoutesForPackage(currentPackage.PackageId);
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            string username = tbRegisterUsername.Text,
                   password = tbRegisterPassword.Text,
                   email = tbRegisterEmail.Text;
            if (!string.IsNullOrWhiteSpace(username) &&
                !string.IsNullOrWhiteSpace(password) &&
                !string.IsNullOrWhiteSpace(email))
            {
                Client currentClient = new Client()
                {
                    UserName = username,
                    Password = password,
                    Email = email
                };
                bool succes=_adminReference.AddUser(currentClient);
                if (!succes)
                {
                    lblError.Text = Resources.MainForm_btnSubmit_Click_Could_not_register_this_username;
                }
                else
                {
                    lblError.Text = "";
                    RegisterPanel.Visible = false;
                }

            }
            else
            {
                lblRegisterError.Text = Resources.MainForm_btnSubmit_Click_Fields_cannot_be_left_empty_;
            }
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            RegisterPanel.Dock = DockStyle.Fill;
            RegisterPanel.Visible = true;
            Usernamelbl.Visible = false;
            passowrdLbl.Visible = false;

        }

        private void addPackagebtn_Click(object sender, EventArgs e)
        {
            AddPackagePanel.Dock=DockStyle.Fill;
            AddPackagePanel.Visible = true;
            cbSender.DataSource = _adminReference.GetAllClientNames();
            cbSender.DropDownStyle=ComboBoxStyle.DropDownList;
            cbReceiver.DataSource = _adminReference.GetAllClientNames();
            cbReceiver.DropDownStyle = ComboBoxStyle.DropDownList;
            cbSender.Text = Resources.MainForm_addPackagebtn_Click_Select_client;
            cbReceiver.Text = Resources.MainForm_addPackagebtn_Click_Select_client;

        }

        private void btnAddPackage_Click(object sender, EventArgs e)
        {
            string  senderClient = cbSender.Text,
                    receiverClient = cbReceiver.Text,
                    name = tbName.Text,
                    description = tbDescription.Text,
                    senderCity = tbSenderCity.Text,
                    receiverCity = tbReceiverCity.Text;


            if (cbSender.Text == Resources.MainForm_addPackagebtn_Click_Select_client)
            {
                lblAddPackageError.Text = Resources.MainForm_btnAddPackage_Click_Please_select_a_sender_;
                return;
            }
            if (cbReceiver.Text == Resources.MainForm_addPackagebtn_Click_Select_client)
            {
                lblAddPackageError.Text = Resources.MainForm_btnAddPackage_Click_Please_select_a_receiver_;
                return;
            }
            if (string.IsNullOrWhiteSpace(tbName.Text))
            {
                lblAddPackageError.Text = Resources.MainForm_btnAddPackage_Click_Name_cannot_be_empty_;
                return;
            }
            if (string.IsNullOrWhiteSpace(tbDescription.Text))
            {
                lblAddPackageError.Text = Resources.MainForm_btnAddPackage_Click_Description_cannot_be_empty_;
                return;
            }
            if (string.IsNullOrWhiteSpace(tbSenderCity.Text) || string.IsNullOrWhiteSpace(tbReceiverCity.Text))
            {
                lblAddPackageError.Text = Resources.MainForm_btnAddPackage_Click_City_cannot_be_empty_;
                return;
            }

            Client senderCl = _adminReference.GetClientByName(senderClient);
            Client receiverCl = _adminReference.GetClientByName(receiverClient);
            Package package =new Package()
            {
                SenderClientId = senderCl.ClientId,
                ReceiverClientId = receiverCl.ClientId,
                Name=name,
                Description = description,
                SenderCity = senderCity,
                DestinationCity = receiverCity
            };
            if (!_adminReference.AddPackage(package))
            {
                lblAddPackageError.Text = Resources.MainForm_btnAddPackage_Click_There_was_an_error_adding_the_package_;
            }
            else
            {
               
                AddPackagePanel.Visible = false;
                _list=new BindingList<PackageDto>(_adminReference.GetAllPackages());
                PackagesGridViedw.DataSource = _list;
            }

        }

        private void btnSubmitPackageStatus_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tbStatusCity.Text)) return;
            _adminReference.UpdatePackageStatus(_currentPackageId,tbStatusCity.Text);
            UpdateStatusPanel.Visible = false;
        }

        private void searchBtn_Click(object sender, EventArgs e)
        {
            string searchText = tbSearch.Text;
            _list = new BindingList<ClientService.PackageDto>(_clientReference.SearchPackages(searchText));
            PackagesGridViedw.DataSource = _list;
        }

        private void tbSearch_TextChanged(object sender, EventArgs e)
        {
            string searchText = tbSearch.Text;
            _list = new BindingList<ClientService.PackageDto>(_clientPackageDtos.Where(p=>p.Name.StartsWith(searchText)).ToList());
            PackagesGridViedw.DataSource = _list;
        }

        private void btnClosePackageStatus_Click(object sender, EventArgs e)
        {
            PackageStatusPanel.Visible = false;
            adminPanel.Visible = true;
        }

        private void SignOutbtn_Click(object sender, EventArgs e)
        {
            adminPanel.Visible = false;

        }
    }
}
