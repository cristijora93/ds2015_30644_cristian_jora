﻿namespace GUI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbUsername = new System.Windows.Forms.TextBox();
            this.tbPassword = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.lblError = new System.Windows.Forms.Label();
            this.adminPanel = new System.Windows.Forms.Panel();
            this.searchBtn = new System.Windows.Forms.Button();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.AddPackagePanel = new System.Windows.Forms.Panel();
            this.lblAddPackageError = new System.Windows.Forms.Label();
            this.btnAddPackage = new System.Windows.Forms.Button();
            this.cbSender = new System.Windows.Forms.ComboBox();
            this.lblSender = new System.Windows.Forms.Label();
            this.cbReceiver = new System.Windows.Forms.ComboBox();
            this.lblReceiver = new System.Windows.Forms.Label();
            this.tbReceiverCity = new System.Windows.Forms.TextBox();
            this.lblReceiverCity = new System.Windows.Forms.Label();
            this.tbSenderCity = new System.Windows.Forms.TextBox();
            this.lblSenderCity = new System.Windows.Forms.Label();
            this.tbDescription = new System.Windows.Forms.TextBox();
            this.lblDescription = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.UpdateStatusPanel = new System.Windows.Forms.Panel();
            this.btnSubmitPackageStatus = new System.Windows.Forms.Button();
            this.tbStatusCity = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.addPackagebtn = new System.Windows.Forms.Button();
            this.PackagesGridViedw = new System.Windows.Forms.DataGridView();
            this.btnRegister = new System.Windows.Forms.Button();
            this.RegisterPanel = new System.Windows.Forms.Panel();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.tbRegisterEmail = new System.Windows.Forms.TextBox();
            this.lblRegisterError = new System.Windows.Forms.Label();
            this.tbRegisterPassword = new System.Windows.Forms.TextBox();
            this.tbRegisterUsername = new System.Windows.Forms.TextBox();
            this.PackageStatusPanel = new System.Windows.Forms.Panel();
            this.btnClosePackageStatus = new System.Windows.Forms.Button();
            this.lblPackageStatus = new System.Windows.Forms.Label();
            this.packageStatusGridView = new System.Windows.Forms.DataGridView();
            this.Usernamelbl = new System.Windows.Forms.Label();
            this.passowrdLbl = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SignOutbtn = new System.Windows.Forms.Button();
            this.adminPanel.SuspendLayout();
            this.AddPackagePanel.SuspendLayout();
            this.UpdateStatusPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PackagesGridViedw)).BeginInit();
            this.RegisterPanel.SuspendLayout();
            this.PackageStatusPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.packageStatusGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // tbUsername
            // 
            this.tbUsername.Location = new System.Drawing.Point(154, 155);
            this.tbUsername.Name = "tbUsername";
            this.tbUsername.Size = new System.Drawing.Size(164, 20);
            this.tbUsername.TabIndex = 0;
            // 
            // tbPassword
            // 
            this.tbPassword.Location = new System.Drawing.Point(154, 190);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.PasswordChar = '*';
            this.tbPassword.Size = new System.Drawing.Size(164, 20);
            this.tbPassword.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(154, 252);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Login";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.Location = new System.Drawing.Point(174, 224);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(0, 13);
            this.lblError.TabIndex = 4;
            // 
            // adminPanel
            // 
            this.adminPanel.Controls.Add(this.SignOutbtn);
            this.adminPanel.Controls.Add(this.searchBtn);
            this.adminPanel.Controls.Add(this.tbSearch);
            this.adminPanel.Controls.Add(this.PackageStatusPanel);
            this.adminPanel.Controls.Add(this.AddPackagePanel);
            this.adminPanel.Controls.Add(this.UpdateStatusPanel);
            this.adminPanel.Controls.Add(this.addPackagebtn);
            this.adminPanel.Controls.Add(this.PackagesGridViedw);
            this.adminPanel.Location = new System.Drawing.Point(43, 59);
            this.adminPanel.Name = "adminPanel";
            this.adminPanel.Size = new System.Drawing.Size(603, 472);
            this.adminPanel.TabIndex = 5;
            this.adminPanel.Visible = false;
            // 
            // searchBtn
            // 
            this.searchBtn.Location = new System.Drawing.Point(446, 15);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(75, 23);
            this.searchBtn.TabIndex = 5;
            this.searchBtn.Text = "Search";
            this.searchBtn.UseVisualStyleBackColor = true;
            this.searchBtn.Click += new System.EventHandler(this.searchBtn_Click);
            // 
            // tbSearch
            // 
            this.tbSearch.Location = new System.Drawing.Point(254, 18);
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(186, 20);
            this.tbSearch.TabIndex = 4;
            this.tbSearch.TextChanged += new System.EventHandler(this.tbSearch_TextChanged);
            // 
            // AddPackagePanel
            // 
            this.AddPackagePanel.Controls.Add(this.lblAddPackageError);
            this.AddPackagePanel.Controls.Add(this.btnAddPackage);
            this.AddPackagePanel.Controls.Add(this.cbSender);
            this.AddPackagePanel.Controls.Add(this.lblSender);
            this.AddPackagePanel.Controls.Add(this.cbReceiver);
            this.AddPackagePanel.Controls.Add(this.lblReceiver);
            this.AddPackagePanel.Controls.Add(this.tbReceiverCity);
            this.AddPackagePanel.Controls.Add(this.lblReceiverCity);
            this.AddPackagePanel.Controls.Add(this.tbSenderCity);
            this.AddPackagePanel.Controls.Add(this.lblSenderCity);
            this.AddPackagePanel.Controls.Add(this.tbDescription);
            this.AddPackagePanel.Controls.Add(this.lblDescription);
            this.AddPackagePanel.Controls.Add(this.tbName);
            this.AddPackagePanel.Controls.Add(this.label1);
            this.AddPackagePanel.Location = new System.Drawing.Point(11, 69);
            this.AddPackagePanel.Name = "AddPackagePanel";
            this.AddPackagePanel.Size = new System.Drawing.Size(457, 312);
            this.AddPackagePanel.TabIndex = 2;
            this.AddPackagePanel.Visible = false;
            // 
            // lblAddPackageError
            // 
            this.lblAddPackageError.AutoSize = true;
            this.lblAddPackageError.Location = new System.Drawing.Point(259, 313);
            this.lblAddPackageError.Name = "lblAddPackageError";
            this.lblAddPackageError.Size = new System.Drawing.Size(0, 13);
            this.lblAddPackageError.TabIndex = 19;
            // 
            // btnAddPackage
            // 
            this.btnAddPackage.Location = new System.Drawing.Point(301, 347);
            this.btnAddPackage.Name = "btnAddPackage";
            this.btnAddPackage.Size = new System.Drawing.Size(103, 30);
            this.btnAddPackage.TabIndex = 18;
            this.btnAddPackage.Text = "Submit";
            this.btnAddPackage.UseVisualStyleBackColor = true;
            this.btnAddPackage.Click += new System.EventHandler(this.btnAddPackage_Click);
            // 
            // cbSender
            // 
            this.cbSender.FormattingEnabled = true;
            this.cbSender.Location = new System.Drawing.Point(262, 93);
            this.cbSender.Name = "cbSender";
            this.cbSender.Size = new System.Drawing.Size(193, 21);
            this.cbSender.TabIndex = 17;
            // 
            // lblSender
            // 
            this.lblSender.AutoSize = true;
            this.lblSender.Location = new System.Drawing.Point(155, 93);
            this.lblSender.Name = "lblSender";
            this.lblSender.Size = new System.Drawing.Size(41, 13);
            this.lblSender.TabIndex = 16;
            this.lblSender.Text = "Sender";
            // 
            // cbReceiver
            // 
            this.cbReceiver.FormattingEnabled = true;
            this.cbReceiver.Location = new System.Drawing.Point(262, 129);
            this.cbReceiver.Name = "cbReceiver";
            this.cbReceiver.Size = new System.Drawing.Size(193, 21);
            this.cbReceiver.TabIndex = 15;
            // 
            // lblReceiver
            // 
            this.lblReceiver.AutoSize = true;
            this.lblReceiver.Location = new System.Drawing.Point(155, 129);
            this.lblReceiver.Name = "lblReceiver";
            this.lblReceiver.Size = new System.Drawing.Size(50, 13);
            this.lblReceiver.TabIndex = 14;
            this.lblReceiver.Text = "Receiver";
            // 
            // tbReceiverCity
            // 
            this.tbReceiverCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbReceiverCity.Location = new System.Drawing.Point(262, 271);
            this.tbReceiverCity.Name = "tbReceiverCity";
            this.tbReceiverCity.Size = new System.Drawing.Size(193, 22);
            this.tbReceiverCity.TabIndex = 13;
            // 
            // lblReceiverCity
            // 
            this.lblReceiverCity.AutoSize = true;
            this.lblReceiverCity.Location = new System.Drawing.Point(155, 276);
            this.lblReceiverCity.Name = "lblReceiverCity";
            this.lblReceiverCity.Size = new System.Drawing.Size(67, 13);
            this.lblReceiverCity.TabIndex = 12;
            this.lblReceiverCity.Text = "ReceiverCity";
            // 
            // tbSenderCity
            // 
            this.tbSenderCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbSenderCity.Location = new System.Drawing.Point(262, 236);
            this.tbSenderCity.Name = "tbSenderCity";
            this.tbSenderCity.Size = new System.Drawing.Size(193, 22);
            this.tbSenderCity.TabIndex = 11;
            // 
            // lblSenderCity
            // 
            this.lblSenderCity.AutoSize = true;
            this.lblSenderCity.Location = new System.Drawing.Point(155, 241);
            this.lblSenderCity.Name = "lblSenderCity";
            this.lblSenderCity.Size = new System.Drawing.Size(58, 13);
            this.lblSenderCity.TabIndex = 10;
            this.lblSenderCity.Text = "SenderCity";
            // 
            // tbDescription
            // 
            this.tbDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbDescription.Location = new System.Drawing.Point(262, 199);
            this.tbDescription.Name = "tbDescription";
            this.tbDescription.Size = new System.Drawing.Size(193, 22);
            this.tbDescription.TabIndex = 9;
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(155, 204);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(60, 13);
            this.lblDescription.TabIndex = 8;
            this.lblDescription.Text = "Description";
            // 
            // tbName
            // 
            this.tbName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbName.Location = new System.Drawing.Point(262, 164);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(193, 22);
            this.tbName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(155, 169);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // UpdateStatusPanel
            // 
            this.UpdateStatusPanel.Controls.Add(this.btnSubmitPackageStatus);
            this.UpdateStatusPanel.Controls.Add(this.tbStatusCity);
            this.UpdateStatusPanel.Controls.Add(this.label2);
            this.UpdateStatusPanel.Location = new System.Drawing.Point(24, 83);
            this.UpdateStatusPanel.Name = "UpdateStatusPanel";
            this.UpdateStatusPanel.Size = new System.Drawing.Size(281, 233);
            this.UpdateStatusPanel.TabIndex = 3;
            this.UpdateStatusPanel.Visible = false;
            // 
            // btnSubmitPackageStatus
            // 
            this.btnSubmitPackageStatus.Location = new System.Drawing.Point(134, 162);
            this.btnSubmitPackageStatus.Name = "btnSubmitPackageStatus";
            this.btnSubmitPackageStatus.Size = new System.Drawing.Size(75, 23);
            this.btnSubmitPackageStatus.TabIndex = 20;
            this.btnSubmitPackageStatus.Text = "Submit";
            this.btnSubmitPackageStatus.UseVisualStyleBackColor = true;
            this.btnSubmitPackageStatus.Click += new System.EventHandler(this.btnSubmitPackageStatus_Click);
            // 
            // tbStatusCity
            // 
            this.tbStatusCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbStatusCity.Location = new System.Drawing.Point(92, 96);
            this.tbStatusCity.Name = "tbStatusCity";
            this.tbStatusCity.Size = new System.Drawing.Size(193, 22);
            this.tbStatusCity.TabIndex = 19;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(45, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "City";
            // 
            // addPackagebtn
            // 
            this.addPackagebtn.Location = new System.Drawing.Point(15, 5);
            this.addPackagebtn.Name = "addPackagebtn";
            this.addPackagebtn.Size = new System.Drawing.Size(119, 33);
            this.addPackagebtn.TabIndex = 1;
            this.addPackagebtn.Text = "Add Package";
            this.addPackagebtn.UseVisualStyleBackColor = true;
            this.addPackagebtn.Click += new System.EventHandler(this.addPackagebtn_Click);
            // 
            // PackagesGridViedw
            // 
            this.PackagesGridViedw.AllowUserToAddRows = false;
            this.PackagesGridViedw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PackagesGridViedw.Location = new System.Drawing.Point(3, 45);
            this.PackagesGridViedw.Name = "PackagesGridViedw";
            this.PackagesGridViedw.Size = new System.Drawing.Size(741, 425);
            this.PackagesGridViedw.TabIndex = 0;
            this.PackagesGridViedw.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1_CellClick);
            // 
            // btnRegister
            // 
            this.btnRegister.Location = new System.Drawing.Point(243, 252);
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.Size = new System.Drawing.Size(75, 23);
            this.btnRegister.TabIndex = 6;
            this.btnRegister.Text = "Register";
            this.btnRegister.UseVisualStyleBackColor = true;
            this.btnRegister.Click += new System.EventHandler(this.btnRegister_Click);
            // 
            // RegisterPanel
            // 
            this.RegisterPanel.Controls.Add(this.label5);
            this.RegisterPanel.Controls.Add(this.label4);
            this.RegisterPanel.Controls.Add(this.label3);
            this.RegisterPanel.Controls.Add(this.btnSubmit);
            this.RegisterPanel.Controls.Add(this.tbRegisterEmail);
            this.RegisterPanel.Controls.Add(this.lblRegisterError);
            this.RegisterPanel.Controls.Add(this.tbRegisterPassword);
            this.RegisterPanel.Controls.Add(this.tbRegisterUsername);
            this.RegisterPanel.Location = new System.Drawing.Point(652, 101);
            this.RegisterPanel.Name = "RegisterPanel";
            this.RegisterPanel.Size = new System.Drawing.Size(590, 529);
            this.RegisterPanel.TabIndex = 7;
            this.RegisterPanel.Visible = false;
            // 
            // btnSubmit
            // 
            this.btnSubmit.Location = new System.Drawing.Point(285, 280);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(75, 23);
            this.btnSubmit.TabIndex = 8;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // tbRegisterEmail
            // 
            this.tbRegisterEmail.Location = new System.Drawing.Point(254, 203);
            this.tbRegisterEmail.Name = "tbRegisterEmail";
            this.tbRegisterEmail.Size = new System.Drawing.Size(156, 20);
            this.tbRegisterEmail.TabIndex = 8;
            // 
            // lblRegisterError
            // 
            this.lblRegisterError.AutoSize = true;
            this.lblRegisterError.Location = new System.Drawing.Point(282, 247);
            this.lblRegisterError.Name = "lblRegisterError";
            this.lblRegisterError.Size = new System.Drawing.Size(0, 13);
            this.lblRegisterError.TabIndex = 7;
            // 
            // tbRegisterPassword
            // 
            this.tbRegisterPassword.Location = new System.Drawing.Point(254, 164);
            this.tbRegisterPassword.Name = "tbRegisterPassword";
            this.tbRegisterPassword.PasswordChar = '*';
            this.tbRegisterPassword.Size = new System.Drawing.Size(156, 20);
            this.tbRegisterPassword.TabIndex = 6;
            // 
            // tbRegisterUsername
            // 
            this.tbRegisterUsername.Location = new System.Drawing.Point(254, 129);
            this.tbRegisterUsername.Name = "tbRegisterUsername";
            this.tbRegisterUsername.Size = new System.Drawing.Size(156, 20);
            this.tbRegisterUsername.TabIndex = 5;
            // 
            // PackageStatusPanel
            // 
            this.PackageStatusPanel.Controls.Add(this.btnClosePackageStatus);
            this.PackageStatusPanel.Controls.Add(this.lblPackageStatus);
            this.PackageStatusPanel.Controls.Add(this.packageStatusGridView);
            this.PackageStatusPanel.Location = new System.Drawing.Point(15, 362);
            this.PackageStatusPanel.Name = "PackageStatusPanel";
            this.PackageStatusPanel.Size = new System.Drawing.Size(630, 425);
            this.PackageStatusPanel.TabIndex = 6;
            this.PackageStatusPanel.Visible = false;
            // 
            // btnClosePackageStatus
            // 
            this.btnClosePackageStatus.Location = new System.Drawing.Point(347, 491);
            this.btnClosePackageStatus.Name = "btnClosePackageStatus";
            this.btnClosePackageStatus.Size = new System.Drawing.Size(111, 32);
            this.btnClosePackageStatus.TabIndex = 2;
            this.btnClosePackageStatus.Text = "Close";
            this.btnClosePackageStatus.UseVisualStyleBackColor = true;
            this.btnClosePackageStatus.Click += new System.EventHandler(this.btnClosePackageStatus_Click);
            // 
            // lblPackageStatus
            // 
            this.lblPackageStatus.AutoSize = true;
            this.lblPackageStatus.Location = new System.Drawing.Point(321, 22);
            this.lblPackageStatus.Name = "lblPackageStatus";
            this.lblPackageStatus.Size = new System.Drawing.Size(97, 13);
            this.lblPackageStatus.TabIndex = 1;
            this.lblPackageStatus.Text = "Status for package";
            // 
            // packageStatusGridView
            // 
            this.packageStatusGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.packageStatusGridView.Location = new System.Drawing.Point(61, 101);
            this.packageStatusGridView.Name = "packageStatusGridView";
            this.packageStatusGridView.Size = new System.Drawing.Size(708, 375);
            this.packageStatusGridView.TabIndex = 0;
            // 
            // Usernamelbl
            // 
            this.Usernamelbl.AutoSize = true;
            this.Usernamelbl.Location = new System.Drawing.Point(83, 158);
            this.Usernamelbl.Name = "Usernamelbl";
            this.Usernamelbl.Size = new System.Drawing.Size(55, 13);
            this.Usernamelbl.TabIndex = 8;
            this.Usernamelbl.Text = "Username";
            // 
            // passowrdLbl
            // 
            this.passowrdLbl.AutoSize = true;
            this.passowrdLbl.Location = new System.Drawing.Point(83, 197);
            this.passowrdLbl.Name = "passowrdLbl";
            this.passowrdLbl.Size = new System.Drawing.Size(53, 13);
            this.passowrdLbl.TabIndex = 9;
            this.passowrdLbl.Text = "Password";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(193, 132);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Username";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(193, 206);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Email";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(193, 167);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Password";
            // 
            // SignOutbtn
            // 
            this.SignOutbtn.Location = new System.Drawing.Point(140, 5);
            this.SignOutbtn.Name = "SignOutbtn";
            this.SignOutbtn.Size = new System.Drawing.Size(108, 33);
            this.SignOutbtn.TabIndex = 7;
            this.SignOutbtn.Text = "Sign Out";
            this.SignOutbtn.UseVisualStyleBackColor = true;
            this.SignOutbtn.Click += new System.EventHandler(this.SignOutbtn_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(949, 618);
            this.Controls.Add(this.passowrdLbl);
            this.Controls.Add(this.Usernamelbl);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.adminPanel);
            this.Controls.Add(this.RegisterPanel);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tbPassword);
            this.Controls.Add(this.btnRegister);
            this.Controls.Add(this.tbUsername);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.adminPanel.ResumeLayout(false);
            this.adminPanel.PerformLayout();
            this.AddPackagePanel.ResumeLayout(false);
            this.AddPackagePanel.PerformLayout();
            this.UpdateStatusPanel.ResumeLayout(false);
            this.UpdateStatusPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PackagesGridViedw)).EndInit();
            this.RegisterPanel.ResumeLayout(false);
            this.RegisterPanel.PerformLayout();
            this.PackageStatusPanel.ResumeLayout(false);
            this.PackageStatusPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.packageStatusGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbUsername;
        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.Panel adminPanel;
        private System.Windows.Forms.DataGridView PackagesGridViedw;
        private System.Windows.Forms.Panel AddPackagePanel;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button addPackagebtn;
        private System.Windows.Forms.Button btnRegister;
        private System.Windows.Forms.Panel RegisterPanel;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.TextBox tbRegisterEmail;
        private System.Windows.Forms.Label lblRegisterError;
        private System.Windows.Forms.TextBox tbRegisterPassword;
        private System.Windows.Forms.TextBox tbRegisterUsername;
        private System.Windows.Forms.Label lblAddPackageError;
        private System.Windows.Forms.Button btnAddPackage;
        private System.Windows.Forms.ComboBox cbSender;
        private System.Windows.Forms.Label lblSender;
        private System.Windows.Forms.ComboBox cbReceiver;
        private System.Windows.Forms.Label lblReceiver;
        private System.Windows.Forms.TextBox tbReceiverCity;
        private System.Windows.Forms.Label lblReceiverCity;
        private System.Windows.Forms.TextBox tbSenderCity;
        private System.Windows.Forms.Label lblSenderCity;
        private System.Windows.Forms.TextBox tbDescription;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Panel UpdateStatusPanel;
        private System.Windows.Forms.Button btnSubmitPackageStatus;
        private System.Windows.Forms.TextBox tbStatusCity;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.Panel PackageStatusPanel;
        private System.Windows.Forms.Label lblPackageStatus;
        private System.Windows.Forms.DataGridView packageStatusGridView;
        private System.Windows.Forms.Button btnClosePackageStatus;
        private System.Windows.Forms.Label Usernamelbl;
        private System.Windows.Forms.Label passowrdLbl;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button SignOutbtn;
    }
}