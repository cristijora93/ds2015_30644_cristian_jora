﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GUI.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("GUI.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select client.
        /// </summary>
        internal static string MainForm_addPackagebtn_Click_Select_client {
            get {
                return ResourceManager.GetString("MainForm_addPackagebtn_Click_Select_client", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to City cannot be empty!.
        /// </summary>
        internal static string MainForm_btnAddPackage_Click_City_cannot_be_empty_ {
            get {
                return ResourceManager.GetString("MainForm_btnAddPackage_Click_City_cannot_be_empty_", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Description cannot be empty!.
        /// </summary>
        internal static string MainForm_btnAddPackage_Click_Description_cannot_be_empty_ {
            get {
                return ResourceManager.GetString("MainForm_btnAddPackage_Click_Description_cannot_be_empty_", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Name cannot be empty!.
        /// </summary>
        internal static string MainForm_btnAddPackage_Click_Name_cannot_be_empty_ {
            get {
                return ResourceManager.GetString("MainForm_btnAddPackage_Click_Name_cannot_be_empty_", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please select a receiver!.
        /// </summary>
        internal static string MainForm_btnAddPackage_Click_Please_select_a_receiver_ {
            get {
                return ResourceManager.GetString("MainForm_btnAddPackage_Click_Please_select_a_receiver_", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please select a sender!.
        /// </summary>
        internal static string MainForm_btnAddPackage_Click_Please_select_a_sender_ {
            get {
                return ResourceManager.GetString("MainForm_btnAddPackage_Click_Please_select_a_sender_", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to There was an error adding the package!.
        /// </summary>
        internal static string MainForm_btnAddPackage_Click_There_was_an_error_adding_the_package_ {
            get {
                return ResourceManager.GetString("MainForm_btnAddPackage_Click_There_was_an_error_adding_the_package_", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Could not register this username.
        /// </summary>
        internal static string MainForm_btnSubmit_Click_Could_not_register_this_username {
            get {
                return ResourceManager.GetString("MainForm_btnSubmit_Click_Could_not_register_this_username", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fields cannot be left empty!.
        /// </summary>
        internal static string MainForm_btnSubmit_Click_Fields_cannot_be_left_empty_ {
            get {
                return ResourceManager.GetString("MainForm_btnSubmit_Click_Fields_cannot_be_left_empty_", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Username or password is incorrect.
        /// </summary>
        internal static string MainForm_button1_Click_Username_or_password_is_incorrect {
            get {
                return ResourceManager.GetString("MainForm_button1_Click_Username_or_password_is_incorrect", resourceCulture);
            }
        }
    }
}
