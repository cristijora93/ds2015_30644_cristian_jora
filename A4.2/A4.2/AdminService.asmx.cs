﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Services;
using DataAcces;
using DataAcces.Models;

namespace A4._2
{
    /// <summary>
    /// Summary description for AdminService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class AdminService : System.Web.Services.WebService
    {
       private DataAccesContext db = new DataAccesContext();
        [WebMethod]
        public bool AddUser(Client client)
        {
            var user = db.Clients.FirstOrDefault(p => p.UserName == client.UserName);
            if (user == null)
            {
                try
                {
                    db.Clients.AddOrUpdate(client);
                    db.SaveChanges();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else return false;
        }

        [WebMethod]
        public bool VerifyUser(string username, string password)
        {
            return db.Clients.FirstOrDefault(c => c.UserName == username && c.Password == password) != null;
        }

        [WebMethod]
        public Client GetUser(string username, string password)
        {
            return db.Clients.FirstOrDefault(c => c.UserName == username && c.Password == password);
        }


        [WebMethod]
        public List<string> GetAllClientNames()
        {
            return db.Clients.Select(c => c.UserName).ToList();
        }

        [WebMethod]
        public Client GetClientByName(string userName)
        {
            return db.Clients.FirstOrDefault(c => c.UserName == userName);
        }

        [WebMethod]
        public List<PackageDto> GetAllPackages()
        {
            var query = from pack in db.Packages
                        select new PackageDto()
                        {
                            PackageId = pack.PackageId,
                            SenderClient = pack.Sender.UserName,
                            ReceiverClient = pack.Receiver.UserName,
                            Name = pack.Name,
                            Description = pack.Description,
                            SenderCity = pack.SenderCity,
                            DestinationCity = pack.DestinationCity,
                            Tracking = pack.Tracking
                        };
            return query.ToList();
        }

        [WebMethod]
        public bool AddPackage(Package package)
        {
            try
            {
                db.Packages.AddOrUpdate(package);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        [WebMethod]
        public bool RemovePackage(int packageId)
        {
            var package = db.Packages.FirstOrDefault(p => p.PackageId == packageId);
            if (package == null) return false;
            db.Packages.Remove(package);
            db.Routes.RemoveRange(db.Routes.Where(r => r.PackageId == packageId));
            db.SaveChanges();
            return true;
        }

        [WebMethod]
        public void RegisterPackage(int packageId)
        {
            var package = db.Packages.FirstOrDefault(p => p.PackageId == packageId);
            if (package == null) return;
            package.Tracking = true;
            var route=new Route()
            {
                PackageId = packageId,
                City=package.SenderCity
            };
            db.Packages.AddOrUpdate(package);
            db.Routes.AddOrUpdate(route);
            db.SaveChanges();
        }

        [WebMethod]
        public void DeleteClients()
        {
            db.Clients.RemoveRange(db.Clients.Where(p => p.ClientId > 0));
            db.Packages.RemoveRange(db.Packages.Where(p => p.SenderClientId > 0));
            db.SaveChanges();
        }

        [WebMethod]
        public void UpdatePackageStatus(int packageId, string city)
        {
            var package = db.Packages.FirstOrDefault(p => p.PackageId == packageId);
            if (package == null) return;
            var route=new Route()
            {
                PackageId = packageId,
                City = city
            };
            db.Routes.Add(route);
            db.SaveChanges();
        }
    }
}
