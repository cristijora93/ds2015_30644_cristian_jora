﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DataAcces;
using DataAcces.Models;

namespace A4._2
{
    /// <summary>
    /// Summary description for ClientService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ClientService : System.Web.Services.WebService
    {
        private DataAccesContext db = new DataAccesContext();
        [WebMethod]
        public List<PackageDto> GetAllPackages(int clientId)
        {
            var query = from pack in db.Packages
                        where (pack.ReceiverClientId==clientId || pack.SenderClientId ==clientId)
                        select new PackageDto()
                        {
                            PackageId = pack.PackageId,
                            SenderClient = pack.Sender.UserName,
                            ReceiverClient = pack.Receiver.UserName,
                            Name = pack.Name,
                            Description = pack.Description,
                            SenderCity = pack.SenderCity,
                            DestinationCity = pack.DestinationCity,
                            Tracking = pack.Tracking
                        };
            return query.ToList();
        }

        [WebMethod]
        public List<Route> GetAllRoutesForPackage(int packageId)
        {
            return db.Routes.Where(route => route.PackageId == packageId).
                                                     OrderBy(r => r.Time).
                                                     ToList();
        }

        [WebMethod]
        public List<PackageDto> SearchPackages(string searchText)
        {
            var query = from pack in db.Packages
                        where pack.Name.StartsWith(searchText)
                        select new PackageDto()
                        {
                            PackageId = pack.PackageId,
                            SenderClient = pack.Sender.UserName,
                            ReceiverClient = pack.Receiver.UserName,
                            Name = pack.Name,
                            Description = pack.Description,
                            SenderCity = pack.SenderCity,
                            DestinationCity = pack.DestinationCity,
                            Tracking = pack.Tracking
                        };
            return query.ToList();
        }
    }
}
