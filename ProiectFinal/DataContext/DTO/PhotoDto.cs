﻿using System.ComponentModel.DataAnnotations;

namespace DataContext.DTO
{
    public class PhotoDto
    {
        public PhotoDto()
        {
            Votes = 0;
        }

        public int PhotoId { get; set; }
        [StringLength(20,ErrorMessage = "Title can be max 20 characters")]
        public string Title { get; set; }
        public string Description { get; set; }
        public int Votes { get; set; }
        public int Views { get; set; }
        public string LargeImagelPath { get; set; }
        public string SmallImagelPath { get; set; }
        public string User{ get; set; }
        public string Category{ get; set; }
    }
}