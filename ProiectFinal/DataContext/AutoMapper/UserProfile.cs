﻿using AutoMapper;
using DataContext.DTO;
using DataContext.Models;

namespace DataContext.AutoMapper
{
    internal class UserProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<User, UserDto>();

        }
    }
}