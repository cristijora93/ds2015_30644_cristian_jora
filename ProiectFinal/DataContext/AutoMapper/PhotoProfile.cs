﻿using System.Collections.Generic;
using AutoMapper;
using DataContext.DTO;
using DataContext.Models;

namespace DataContext.AutoMapper
{ 
    internal class PhotoProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<IEnumerable<Photo>, IEnumerable<PhotoDto>>();
            Mapper.CreateMap<Photo, PhotoDto>()
               .ForMember(dto => dto.Category, conf => conf.MapFrom(ol => ol.Category.Name))
               .ForMember(dto => dto.User, conf => conf.MapFrom(ol => ol.User.FirstName));
        }
    }
}