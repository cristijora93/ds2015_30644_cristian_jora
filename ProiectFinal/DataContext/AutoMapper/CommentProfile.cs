﻿using System.Collections.Generic;
using AutoMapper;
using DataContext.DTO;
using DataContext.Models;

namespace DataContext.AutoMapper
{
    internal class CommentProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<IEnumerable<Comment>, IEnumerable<CommentDto>>();
            Mapper.CreateMap<Comment, CommentDto>()
                .ForMember(dto => dto.User, conf => conf.MapFrom(ol => ol.User.UserName));
        }
    }
}