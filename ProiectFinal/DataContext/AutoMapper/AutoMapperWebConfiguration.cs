﻿using AutoMapper;
namespace DataContext.AutoMapper
{
    public class AutoMapperWebConfiguration
    {
        public static void Configure()
        {
          Mapper.Initialize(cfg =>
            {
                cfg.AddProfile(new UserProfile());
                cfg.AddProfile(new PhotoProfile());
                cfg.AddProfile(new CategoryProfile());
                cfg.AddProfile(new CommentProfile());
                cfg.AddProfile(new KeywordProfile());
            });

        }
    }
}