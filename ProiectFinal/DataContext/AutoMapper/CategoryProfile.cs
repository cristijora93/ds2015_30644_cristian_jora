﻿using System.Collections.Generic;
using AutoMapper;
using DataContext.DTO;
using DataContext.Models;

namespace DataContext.AutoMapper
{
    internal class CategoryProfile : Profile
    {
        protected override void Configure()
        {    
            Mapper.CreateMap<Category, CategoryDto>();
            Mapper.CreateMap<CategoryDto, Category>().ForMember(dest =>dest.Photos, ol=>ol.Ignore());
        }
    }
}