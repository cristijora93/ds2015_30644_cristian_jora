﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataContext.Models;

namespace DataContext
{
    public class DataAccesContext:DbContext
    {
        public DbSet<User> Users { get; set; } 
        public DbSet<Category> Categories { get; set; } 
        public DbSet<Keyword> Keywords { get; set; } 
        public DbSet<Comment> Comments { get; set; } 
        public DbSet<Photo> Photos { get; set; } 
    }
}
