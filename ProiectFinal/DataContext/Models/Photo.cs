﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataContext.Models
{
    public class Photo
    {
        public Photo()
        {
            Votes = 0;
        }
        public int PhotoId { get; set; }
      //  [StringLength(20,ErrorMessage = "Title can be max 20 characters")]
        public string Title { get; set; }   
        public string Description { get; set; }   
        public string LargeImagelPath { get; set; }
        public string SmallImagelPath { get; set; }
        public int Votes { get; set; }
        public int Views { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public virtual Category Category { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Keyword> Keywords { get; set; } 
            
    }
}
