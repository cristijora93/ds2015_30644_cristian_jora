﻿using System.Collections.Generic;

namespace DataContext.Models
{
    public class Category
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public ICollection<Photo> Photos { get; set; }
            
    }
}