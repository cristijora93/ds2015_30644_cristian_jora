﻿using System.Collections.Generic;

namespace DataContext.Models
{
    public class Keyword
    {
        public int KeywordId { get; set; } 
        public int Text { get; set; }
        public ICollection<Photo> Photos { get; set; }  
    }
}