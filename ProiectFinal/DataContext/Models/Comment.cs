﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataContext.Models
{
    public class Comment
    {
        public Comment()
        {
            UpdatedDate = DateTime.Now;
        }
        public int CommentId { get; set; }
        public string Text { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UserId { get; set; }
        public int PhotoId { get; set; }
        public Photo Photo { get; set; }
        public User User { get; set; } 
    }
}