﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataContext.Models
{
    public class User
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
     //   [EmailAddress]
        public string Email { get; set; }
      //  [StringLength(255,ErrorMessage = "Password length must be at least 6 characters"),MinLength(6)]
        public string Password { get; set; }

        public ICollection<Photo> Photos{ get; set;}
    }
}