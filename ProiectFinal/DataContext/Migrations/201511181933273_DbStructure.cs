namespace DataContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DbStructure : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        CategoryId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.CategoryId);
            
            CreateTable(
                "dbo.Photos",
                c => new
                    {
                        PhotoId = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                        LargeImagelPath = c.String(),
                        SmallImagelPath = c.String(),
                        Votes = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        Category_CategoryId = c.Int(),
                    })
                .PrimaryKey(t => t.PhotoId)
                .ForeignKey("dbo.Categories", t => t.Category_CategoryId)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.Category_CategoryId);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        CommentId = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        UpdatedDate = c.DateTime(nullable: false),
                        UserId = c.Int(nullable: false),
                        PhotoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CommentId)
                .ForeignKey("dbo.Photos", t => t.PhotoId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId)
                .Index(t => t.PhotoId);
            
            CreateTable(
                "dbo.Keywords",
                c => new
                    {
                        KeywordId = c.Int(nullable: false, identity: true),
                        Text = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.KeywordId);
            
            CreateTable(
                "dbo.KeywordPhotoes",
                c => new
                    {
                        Keyword_KeywordId = c.Int(nullable: false),
                        Photo_PhotoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Keyword_KeywordId, t.Photo_PhotoId })
                .ForeignKey("dbo.Keywords", t => t.Keyword_KeywordId, cascadeDelete: true)
                .ForeignKey("dbo.Photos", t => t.Photo_PhotoId, cascadeDelete: true)
                .Index(t => t.Keyword_KeywordId)
                .Index(t => t.Photo_PhotoId);
            
            AddColumn("dbo.Users", "Password", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.KeywordPhotoes", "Photo_PhotoId", "dbo.Photos");
            DropForeignKey("dbo.KeywordPhotoes", "Keyword_KeywordId", "dbo.Keywords");
            DropForeignKey("dbo.Comments", "UserId", "dbo.Users");
            DropForeignKey("dbo.Photos", "UserId", "dbo.Users");
            DropForeignKey("dbo.Comments", "PhotoId", "dbo.Photos");
            DropForeignKey("dbo.Photos", "Category_CategoryId", "dbo.Categories");
            DropIndex("dbo.KeywordPhotoes", new[] { "Photo_PhotoId" });
            DropIndex("dbo.KeywordPhotoes", new[] { "Keyword_KeywordId" });
            DropIndex("dbo.Comments", new[] { "PhotoId" });
            DropIndex("dbo.Comments", new[] { "UserId" });
            DropIndex("dbo.Photos", new[] { "Category_CategoryId" });
            DropIndex("dbo.Photos", new[] { "UserId" });
            DropColumn("dbo.Users", "Password");
            DropTable("dbo.KeywordPhotoes");
            DropTable("dbo.Keywords");
            DropTable("dbo.Comments");
            DropTable("dbo.Photos");
            DropTable("dbo.Categories");
        }
    }
}
