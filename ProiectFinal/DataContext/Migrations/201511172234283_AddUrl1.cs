namespace DataContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUrl1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
               "dbo.Users",
               c => new
               {
                   UserId = c.Int(nullable: false, identity: true),
                   FirstName = c.String(),
                   LastName = c.String(),
                   Email = c.String(),
               })
               .PrimaryKey(t => t.UserId);
        }
        
        public override void Down()
        {
            DropTable("dbo.Users");
        }
    
    }
}
