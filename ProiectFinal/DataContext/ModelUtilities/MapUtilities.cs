﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DataContext.DTO;
using DataContext.Models;

namespace DataContext.ModelUtilities
{
    public class MapUtilities
    {
        public static void CreateMapForPhoto(User user, Category category)
        {
            Mapper.CreateMap<PhotoDto, Photo>().ForMember(dest => dest.UserId, opts => opts.MapFrom(src => user.UserId))
                .ForMember(dest => dest.User, opts => opts.MapFrom(src => user))
                .ForMember(dest => dest.Category, opts => opts.MapFrom(src => category));
        }

        public static void CreateMapForComment(User user)
        {
            Mapper.CreateMap<CommentDto, Comment>().ForMember(dest => dest.UserId, opts => opts.MapFrom(src => user.UserId))
                                               .ForMember(dest => dest.User, opts => opts.MapFrom(src => user));
        }

    }
}