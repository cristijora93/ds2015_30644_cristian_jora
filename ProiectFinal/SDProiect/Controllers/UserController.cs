﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Http;
using DataContext;
using DataContext.DTO;
using DataContext.Models;
using AutoMapper;
using AutoMapper.QueryableExtensions;

namespace SDProiect.Controllers
{
    public class UserController : ApiController
    {
        private readonly DataAccesContext _db = new DataAccesContext();
        // GET: api/User
        public IEnumerable<UserDto> Get()
        {
            var query = _db.Users.AsQueryable();
            return query.ProjectTo<UserDto>().ToList();
        }

        // GET: api/User/5
        public UserDto Get(int id)
        {
            User user = _db.Users.FirstOrDefault(u => u.UserId == id);
            UserDto result = Mapper.Map<User, UserDto>(user);
            return result;
        }

        // POST: api/User
        public async Task<HttpResponseMessage> Post()
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            Request.Content.LoadIntoBufferAsync().Wait();
            await Request.Content.ReadAsMultipartAsync(new MultipartMemoryStreamProvider()).ContinueWith(task =>
             {
                 MultipartMemoryStreamProvider provider = task.Result;
                 foreach (HttpContent content in provider.Contents)
                 {
                     var testName = content.Headers.ContentDisposition.Name;
                     if (testName != "\"image\"")
                     {
                         break;
                     }
                     Stream stream = content.ReadAsStreamAsync().Result;
                     var image = Image.FromStream(stream);
                   
                     var filePath = HostingEnvironment.MapPath("~/App_Data/Images");
                     var fullPath = Path.Combine(filePath, "test" + new Random(5).Next().ToString() + ".jpg");
                     image.Save(fullPath);
                 }
             });

            return new HttpResponseMessage(HttpStatusCode.BadRequest);
        }
        public HttpStatusCode Delete(int id)
        {
            var query = _db.Users.FirstOrDefault(o => o.UserId == id);
            if (query != null)
            {
                _db.Users.Remove(query);
                _db.SaveChanges();
                return HttpStatusCode.OK;
            }
            else
            {
                return HttpStatusCode.NotFound;
            }

        }


    }
}
