﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Http;
using AutoMapper.QueryableExtensions;
using DataContext;
using DataContext.DTO;

namespace SDProiect.Controllers
{
    public class PhotoController : ApiController
    {
        private readonly DataAccesContext _db = new DataAccesContext();
        // GET: api/Photo
        public IEnumerable<PhotoDto> Get()
        {
            var query = _db.Photos.AsQueryable();
            return query.ProjectTo<PhotoDto>().ToList();

        }

        // GET: api/Photo/5
        public PhotoDto Get(int id)
        {
            var query = _db.Photos.Where(o => o.PhotoId == id);
            if (query != null)
            {
                var photo = query.FirstOrDefault();
                if (photo != null)
                {
                    photo.Views++;
                    _db.Photos.AddOrUpdate(photo);
                    _db.SaveChanges();
                }
               
            }
            return query.ProjectTo<PhotoDto>().FirstOrDefault();
        }


        public async Task<HttpResponseMessage>  Post()
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            Request.Content.LoadIntoBufferAsync().Wait();
            await Request.Content.ReadAsMultipartAsync(new MultipartMemoryStreamProvider()).ContinueWith(task =>
            {
                MultipartMemoryStreamProvider provider = task.Result;
                foreach (HttpContent content in provider.Contents)
                {
                    var testName = content.Headers.ContentDisposition.Name;
                    if (testName != "\"image\"")
                    {
                        break;
                    }
                    Stream stream = content.ReadAsStreamAsync().Result;
                    var image = Image.FromStream(stream);

                    var filePath = HostingEnvironment.MapPath("~/App_Data/Images");
                    var fullPath = Path.Combine(filePath, "test" + new Random().Next().ToString() + ".jpg");
                    image.Save(fullPath);
                }
            });

            return new HttpResponseMessage(HttpStatusCode.BadRequest);
        }

        public HttpStatusCode Delete(int id)
        {
            var query = _db.Photos.FirstOrDefault(o => o.PhotoId == id);
            if (query != null)
            {
                _db.Photos.Remove(query);
                _db.SaveChanges();
                return HttpStatusCode.OK;
            }
            else
            {
                return HttpStatusCode.NotFound;
            }

        }

    }
}

