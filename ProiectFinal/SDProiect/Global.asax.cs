﻿using System.Web.Http;
using DataContext.AutoMapper;
namespace SDProiect
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            AutoMapperWebConfiguration.Configure();
        }
    }
}
